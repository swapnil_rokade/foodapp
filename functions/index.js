const functions = require('firebase-functions');
const admin = require('firebase-admin');
const request = require('request');
const schedule = require('node-schedule');
const nodemailer = require('nodemailer');

admin.initializeApp();
var db = admin.firestore();


//Important for firebase-functions 1.0.1 https://libraries.io/github/firebase/functions-samples and https://github.com/firebase/functions-samples/search?utf8=%E2%9C%93&q=.ref&type=
//require('dotenv').load();
const twilio = require('twilio');

const firebaseConfig = JSON.parse(process.env.FIREBASE_CONFIG);

//const accountSid = firebaseConfig.twilio.sid;
//const authToken  = firebaseConfig.twilio.token;

//const accountSid = "ACc9d490e348ec0b1e8bf737e2a3d5f3de";
//const authToken  = "23b0adf42035640ec7e86dc621351114";

//const express = require('express');
const VoiceResponse = require('twilio').twiml.VoiceResponse;

//const app = express();

const accountSid = "ACa3037026b81499e206a84fe2a228fade";
const authToken  = "a242d6d5e004dfe8d7a370d7b89ba34f";


const client = new twilio(accountSid, authToken);

//const twilioNumber = '+13073171164'; // your twilio phone number
const twilioNumber = '+19082937304'; // your twilio phone number

const inArray = require('in-array');

const braintree = require("braintree");

const gateway = braintree.connect({
					environment: braintree.Environment.Sandbox,
					merchantId: "4dprh62kmhdc4qj6",
					publicKey: "kq29khghy4mw38dn",
					privateKey: "eda57562a3fdb0740dcf698098030af0"
				});


exports.orderEntryCheck = functions.firestore
	.document('/Root/Development/Orders/Orders_Details/Order_Details/{orderId}')
    .onCreate((snap, context) => {
			
			const orderID = context.params.orderId;
			const orderValue = snap.data();
			//Get Restaurant ID code start
			
			const promises = [];
	
			/*var query = db.collection('Root').doc('Development').collection('Orders').doc('Orders_Menus').collection('Order_Menus').where('order_id', '==', orderID).limit(1)
			  .get()
			  .then(snapshot => {
				snapshot.forEach(order => {
					var order_data = order.data();*/
					//console.log('Order Data',order_data);
					if(orderValue.restaurantID){
						var rest_id = orderValue.restaurantID;
						console.log("Restaurant ID",rest_id);
                        
promises.push(db.collection('Root').doc('Development').collection('Users_Restaurants').doc(rest_id).collection(rest_id).doc('tokenFCM').collection("Android")
						  .get()
						  .then(fcmSnapshot => {
							fcmSnapshot.forEach(fcmChild => {
									var fcmtoken = fcmChild.data();
									var fcmKey = fcmChild.id;
								
									//console.log("Andorid FCM TOken",fcmtoken);
									//console.log("Andorid FCM Key",fcmKey);
									var customer_id = orderValue.userID;
									
									db.collection('Root').doc('Development').collection('Users_Consumers').doc(customer_id)
									  .get()
									  .then(custSnapshot => {
											var cust_snap_data = custSnapshot.data();
											console.log("Customer Data orderEntryCheck", cust_snap_data);
											const payload = {
											  notification: {
													"title" : "New Order Created",
													"body" : "You have received a new order from "+cust_snap_data.name+". Please check FoodKonnect app to check & confirm the order : "+ orderID
												},
											  data : {
												"orderId" : orderID
											  }
											};

											//console.log("Android Payload", payload);
											//console.log("Android REST ID", rest_id);
											//console.log("Android Device Token", fcmtoken);

											admin.messaging().sendToDevice(fcmKey, payload);	
									return console.log('Oredr push executed successfully');
								}).catch(error => console.log('Couldnt send push notification', error));			
							});
							return console.log('Function executed successfully here');  
						  })
						);			  
					}
				/*});
				return console.log('Main function executed successfully');	
		  	}).catch(error => console.log('Couldnt send notification', error));*/
        	
				
		return Promise.all(promises);
});

exports.orderEntryCheckLive = functions.firestore
	.document('/Root/Live/Orders/Orders_Details/Order_Details/{orderId}')
    .onCreate((snap, context) => {
			
			const orderID = context.params.orderId;
			const orderValue = snap.data();
			//Get Restaurant ID code start
			
			const promises = [];
	
			/*var query = db.collection('Root').doc('Live').collection('Orders').doc('Orders_Menus').collection('Order_Menus').where('order_id', '==', orderID).limit(1)
			  .get()
			  .then(snapshot => {
				snapshot.forEach(order => {
					var order_data = order.data();*/
					//console.log('Order Data',order_data);
					if(orderValue.restaurantID){
						var rest_id = orderValue.restaurantID;
						console.log("Restaurant ID",rest_id);
                        
promises.push(db.collection('Root').doc('Live').collection('Users_Restaurants').doc(rest_id).collection(rest_id).doc('tokenFCM').collection("Android")
						  .get()
						  .then(fcmSnapshot => {
							fcmSnapshot.forEach(fcmChild => {
									var fcmtoken = fcmChild.data();
									var fcmKey = fcmChild.id;
								
									//console.log("Andorid FCM TOken",fcmtoken);
									//console.log("Andorid FCM Key",fcmKey);
									var customer_id = orderValue.userID;
									
									db.collection('Root').doc('Live').collection('Users_Consumers').doc(customer_id)
									  .get()
									  .then(custSnapshot => {
											var cust_snap_data = custSnapshot.data();
											console.log("Customer Data orderEntryCheck", cust_snap_data);
											const payload = {
											  notification: {
													"title" : "New Order Created",
													"body" : "You have received a new order from "+cust_snap_data.name+". Please check FoodKonnect app to check & confirm the order : "+ orderID
												},
											  data : {
												"orderId" : orderID
											  }
											};

											//console.log("Android Payload", payload);
											//console.log("Android REST ID", rest_id);
											//console.log("Android Device Token", fcmtoken);

											admin.messaging().sendToDevice(fcmKey, payload);	
									return console.log('Oredr push executed successfully');
								}).catch(error => console.log('Couldnt send push notification', error));			
							});
							return console.log('Function executed successfully here');  
						  })
						);			  
					}
				/*});
				return console.log('Main function executed successfully');	
		  	}).catch(error => console.log('Couldnt send notification', error));*/
        	
				
		return Promise.all(promises);
});

exports.orderSMS = functions.firestore
	.document('/Root/Development/Orders/Orders_Details/Order_Details/{orderId}')
    .onCreate((snap, context) => {
			
			const orderID = context.params.orderId;
			const order_data = snap.data();	
			//console.log('Order ID', orderID);
			//Get Restaurant ID code start
			
			const promises = [];
	
			/*var query = db.collection('Root').doc('Development').collection('Orders').doc('Orders_Menus').collection('Order_Menus').where('order_id', '==', orderID).limit(1)
			  .get()
			  .then(snapshot => {
				snapshot.forEach(order => {
					var order_data = order.data();*/
					//console.log('Order Data',order_data);
					if(order_data.restaurantID){
						var rest_id = order_data.restaurantID;
						//console.log("Restaurant ID",rest_id);
						
						//promises.push(db.collection('Root').doc('Development').collection('Users').doc('Restaurants').collection(rest_id).doc('user_profile')	
						promises.push(db.collection('Root').doc('Development').collection('Users_Restaurants').doc(rest_id)			  
						  .get()
						  .then(restSnapshot => {
							  var rest_snap_data = restSnapshot.data();
							  console.log("REst dat",rest_snap_data);
								
								if(rest_snap_data.phone_1){
									var phoneNumber = '';
									
									if((inArray(['9325012281','9009241741','8983633095','9923673679','9922238594','9649478800','9823057870','9582211120','7058429017'], rest_snap_data.phone_1))){
										phoneNumber = "+91"+rest_snap_data.phone_1;	
									}else{
										phoneNumber = "+1"+rest_snap_data.phone_1;
									}
									
									//const phoneNumber = '+919325012281';

									console.log('Phone no',phoneNumber);
									if(phoneNumber){
										if ( !validE164(phoneNumber) ) {
											throw new Error('number must be E164 format!')
										}
				
									var customer_id = order_data.userID;
									db.collection('Root').doc('Development').collection('Users_Consumers').doc(customer_id)
									  .get()
									  .then(custSnapshot => {
											var cust_snap_data = custSnapshot.data();
											console.log("Customer Data send sms", cust_snap_data);
											
											const textMessage = {
												body: 'You have received a new order from '+cust_snap_data.name+'. Please check FoodKonnect app to check & confirm the order : '+ orderID,
												to: phoneNumber,  // Text to this number
												from: twilioNumber // From a valid Twilio number
											}
											console.log('textMessage',textMessage);
											client.messages.create(textMessage);
										return console.log('SMS executed successfully');
									}).catch(error => console.log('Couldnt send SMS', error));		
										
										//url: "https://handler.twilio.com/twiml/EH159462d869f325e4c9e3026f17e9f707?orderId="+ orderID,
										
										client.calls.create({
											url: "https://us-central1-foodkonnect-65adf.cloudfunctions.net/addMessage?orderId="+ orderID,
											to: phoneNumber,  // Text to this number
											from: twilioNumber // From a valid Twilio number
										},function(err,call){
											if(err){
												console.log("Error",err);
											}else{
												console.log("Call Status",call.sid);
											}
										});  
									}
								}
							 return console.log('Function executed successfully'); 
						  })
						);			  

					}
				/*});
				return console.log('Phone function executed successfully');	
		  	}).catch(error => console.log('Couldnt send notification', error));*/
        	
				
		return Promise.all(promises);
});


exports.orderSMSLive = functions.firestore
	.document('/Root/Live/Orders/Orders_Details/Order_Details/{orderId}')
    .onCreate((snap, context) => {
			
			const orderID = context.params.orderId;
			const order_data = snap.data();	
			//console.log('Order ID', orderID);
			//Get Restaurant ID code start
			
			const promises = [];
	
			/*var query = db.collection('Root').doc('Live').collection('Orders').doc('Orders_Menus').collection('Order_Menus').where('order_id', '==', orderID).limit(1)
			  .get()
			  .then(snapshot => {
				snapshot.forEach(order => {
					var order_data = order.data();*/
					//console.log('Order Data',order_data);
					if(order_data.restaurantID){
						var rest_id = order_data.restaurantID;
						//console.log("Restaurant ID",rest_id);
						
						//promises.push(db.collection('Root').doc('Live').collection('Users').doc('Restaurants').collection(rest_id).doc('user_profile')	
						promises.push(db.collection('Root').doc('Live').collection('Users_Restaurants').doc(rest_id)			  
						  .get()
						  .then(restSnapshot => {
							  var rest_snap_data = restSnapshot.data();
							  console.log("REst dat",rest_snap_data);
								
								if(rest_snap_data.phone_1){
									var phoneNumber = '';
									
									if((inArray(['9325012281','9009241741','8983633095','9923673679','9922238594','9649478800','9823057870','9582211120','7058429017'], rest_snap_data.phone_1))){
										phoneNumber = "+91"+rest_snap_data.phone_1;	
									}else{
										phoneNumber = "+1"+rest_snap_data.phone_1;
									}
									
									//const phoneNumber = '+919325012281';

									console.log('Phone no',phoneNumber);
									if(phoneNumber){
										if ( !validE164(phoneNumber) ) {
											throw new Error('number must be E164 format!')
										}
				
									var customer_id = order_data.userID;
									db.collection('Root').doc('Live').collection('Users_Consumers').doc(customer_id)
									  .get()
									  .then(custSnapshot => {
											var cust_snap_data = custSnapshot.data();
											console.log("Customer Data send sms", cust_snap_data);
											
											const textMessage = {
												body: 'You have received a new order from '+cust_snap_data.name+'. Please check FoodKonnect app to check & confirm the order : '+ orderID,
												to: phoneNumber,  // Text to this number
												from: twilioNumber // From a valid Twilio number
											}
											console.log('textMessage',textMessage);
											client.messages.create(textMessage);
										return console.log('SMS executed successfully');
									}).catch(error => console.log('Couldnt send SMS', error));		
										
										//url: "https://handler.twilio.com/twiml/EH159462d869f325e4c9e3026f17e9f707?orderId="+ orderID,
										
										client.calls.create({
											url: "https://us-central1-foodkonnect-65adf.cloudfunctions.net/addMessageLive?orderId="+ orderID,
											to: phoneNumber,  // Text to this number
											from: twilioNumber // From a valid Twilio number
										},function(err,call){
											if(err){
												console.log("Error",err);
											}else{
												console.log("Call Status",call.sid);
											}
										});  
									}
								}
							 return console.log('Function executed successfully'); 
						  })
						);			  

					}
				/*});
				return console.log('Phone function executed successfully');	
		  	}).catch(error => console.log('Couldnt send notification', error));*/
        	
				
		return Promise.all(promises);
});

exports.orderPhone = functions.firestore
	.document('/Root/Development/Orders/Orders_Details/Order_Details/{orderId}')
    .onCreate((snap, context) => {
			
			const orderID = context.params.orderId;
			const order_data = snap.data();	
			//console.log('Order ID', orderID);
			//Get Restaurant ID code start
			
			const promises = [];
	
			/*var query = db.collection('Root').doc('Development').collection('Orders').doc('Orders_Menus').collection('Order_Menus').where('order_id', '==', orderID).limit(1)
			  .get()
			  .then(snapshot => {
				snapshot.forEach(order => {
					var order_data = order.data();*/
					//console.log('Order Data',order_data);
					if(order_data.restaurantID){
						var rest_id = order_data.restaurantID;
						//console.log("Restaurant ID",rest_id);
						
						//promises.push(db.collection('Root').doc('Development').collection('Users').doc('Restaurants').collection(rest_id).doc('user_profile')
						promises.push(db.collection('Root').doc('Development').collection('Users_Restaurants').doc(rest_id)
						  .get()
						  .then(restSnapshot => {
							  var rest_snap_data = restSnapshot.data();
							  console.log("REst dat",rest_snap_data);
								
								if(rest_snap_data.phone_1){
									var phoneNumber = '';
									
									if((inArray(['9325012281','9009241741','8983633095','9923673679','9922238594','9649478800','9823057870','9582211120','7058429017'], rest_snap_data.phone_1))){
										phoneNumber = "+91"+rest_snap_data.phone_1;	
									}else{
										phoneNumber = "+1"+rest_snap_data.phone_1;
									}
									
									//const phoneNumber = '+919325012281';

									console.log('Phone no',phoneNumber);
									if(phoneNumber){
										var duration = 60000;
										console.log('Before sleep');
										
										sleep(duration);
										
										console.log('After sleep');
										
									/*db.collection('Root').doc('Development').collection('Orders').doc('Orders_Details').collection('Order_Details').doc(orderID)
									  .get()
									  .then(orderSnapshot => {
											orderSnapshot.forEach(orderPhone => {
											 	var order_snap_data = orderPhone.data();
							  					console.log("Order data",order_snap_data);*/
											if(order_data.orderStatus === 0){

												client.calls.create({
													url: "https://us-central1-foodkonnect-65adf.cloudfunctions.net/addMessage?orderId="+ orderID,
													to: phoneNumber,  // Text to this number
													from: twilioNumber // From a valid Twilio number
												},function(err,call){
													if(err){
														console.log("Error",err);
													}else{
														console.log("Call Status",call.sid);
													}
												});  
											}
											/*});		
											return console.log('Phone function executed'); 
										}).catch(error => console.log('Couldnt make call', error));*/
										
										
									}
								}
							 return console.log('Function executed successfully'); 
						  })
						);			  

					}
				/*});
				return console.log('Phone function executed successfully');	
		  	}).catch(error => console.log('Couldnt send notification', error));*/
				
		return Promise.all(promises);
});

exports.orderPhoneLive = functions.firestore
	.document('/Root/Live/Orders/Orders_Details/Order_Details/{orderId}')
    .onCreate((snap, context) => {
			
			const orderID = context.params.orderId;
			const order_data = snap.data();	
			//console.log('Order ID', orderID);
			//Get Restaurant ID code start
			
			const promises = [];
	
			/*var query = db.collection('Root').doc('Live').collection('Orders').doc('Orders_Menus').collection('Order_Menus').where('order_id', '==', orderID).limit(1)
			  .get()
			  .then(snapshot => {
				snapshot.forEach(order => {
					var order_data = order.data();*/
					//console.log('Order Data',order_data);
					if(order_data.restaurantID){
						var rest_id = order_data.restaurantID;
						//console.log("Restaurant ID",rest_id);
						
						//promises.push(db.collection('Root').doc('Live').collection('Users').doc('Restaurants').collection(rest_id).doc('user_profile')
						promises.push(db.collection('Root').doc('Live').collection('Users_Restaurants').doc(rest_id)
						  .get()
						  .then(restSnapshot => {
							  var rest_snap_data = restSnapshot.data();
							  console.log("REst dat",rest_snap_data);
								
								if(rest_snap_data.phone_1){
									var phoneNumber = '';
									
									if((inArray(['9325012281','9009241741','8983633095','9923673679','9922238594','9649478800','9823057870','9582211120','7058429017'], rest_snap_data.phone_1))){
										phoneNumber = "+91"+rest_snap_data.phone_1;	
									}else{
										phoneNumber = "+1"+rest_snap_data.phone_1;
									}
									
									//const phoneNumber = '+919325012281';

									console.log('Phone no',phoneNumber);
									if(phoneNumber){
										var duration = 60000;
										console.log('Before sleep');
										
										sleep(duration);
										
										console.log('After sleep');
										
									/*db.collection('Root').doc('Live').collection('Orders').doc('Orders_Details').collection('Order_Details').doc(orderID)
									  .get()
									  .then(orderSnapshot => {
											orderSnapshot.forEach(orderPhone => {
											 	var order_snap_data = orderPhone.data();
							  					console.log("Order data",order_snap_data);*/
											if(order_data.orderStatus === 0){

												client.calls.create({
													url: "https://us-central1-foodkonnect-65adf.cloudfunctions.net/addMessageLive?orderId="+ orderID,
													to: phoneNumber,  // Text to this number
													from: twilioNumber // From a valid Twilio number
												},function(err,call){
													if(err){
														console.log("Error",err);
													}else{
														console.log("Call Status",call.sid);
													}
												});  
											}
											/*});		
											return console.log('Phone function executed'); 
										}).catch(error => console.log('Couldnt make call', error));*/
										
										
									}
								}
							 return console.log('Function executed successfully'); 
						  })
						);			  

					}
				/*});
				return console.log('Phone function executed successfully');	
		  	}).catch(error => console.log('Couldnt send notification', error));*/
				
		return Promise.all(promises);
});

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e30; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}	

/*exports.orderSendfax = functions.firestore
	.document('/Root/Development/Orders/Orders_Details/Order_Details/{orderId}')
    .onCreate((snap, context) => {
		
		
			const promises = [];
			var order_data = [];
			var rest_id = '';
			var customer_id = '';
			var products = [];
	        var order_subtotal = 0;
			var order_tax = 0;
			var order_tip = 0;
			var order_total = 0;
			var order_date = 0;
			var order_eaten = '';
			var order_taken = '';
			var order_delivery = '';
			var order_id_show = '';
			var special_instructions = '';
			var customisation = [];
			var rest_snap_data;
			var cust_snap_data;
	
			var query = db.collection('Root').doc('Development').collection('Orders').doc('Orders_Menus').collection('Order_Menus').where('order_id', '==', orderID)
			  .get()
			  .then(snapshot => {
				snapshot.forEach(order => {
					order_data = order.data();
					
					if(order_data.restaurantID){
						rest_id = order_data.restaurantID;
						customer_id = order_data.userID;
						products.push({product_name:order_data.name,product_count:order_data.count,product_price:order_data.price});
						special_instructions = order_data.instruction;
						customisation.push(order_data.selectedCustomizedLabel);
						//console.log('Customization value',order_data.selectedCustomizedLabel);
						//console.log('Customization',customisation);
					}
				});
				  
				if(rest_id !== '' && customer_id !== ''){
					db.collection('Root').doc('Development').collection('Orders').doc('Orders_Details').collection('Order_Details').doc(orderID)
						  .get()
						  .then(orderdetails => {
							var order_detail_data = orderdetails.data();
							order_subtotal = order_detail_data.subTotal;
							order_tax = order_detail_data.tax;
							order_tip = order_detail_data.tip;
							order_total = order_detail_data.grandTotal;
							order_date = order_detail_data.order_date;
							order_id_show = order_detail_data.order_id_show;
							//order_id_show = "Chan1536582038768"	;
						
							return console.log('Order Details',order_detail_data); 
					}).catch(error => console.log('Couldnt get order details', error));	
					var pdfCount = 0;
					
					db.collection('Root').doc('Development').collection('Users_Consumers').doc(customer_id)
										  .get()
										  .then(custSnapshot => {
											cust_snap_data = custSnapshot.data();
											return console.log('Consumer Snapshot',cust_snap_data); 		
						}).catch(consuError => console.log('User consumer error', consuError));
					
					
					db.collection('Root').doc('Development').collection('Users_Restaurants').doc(rest_id)
						  .get()
						  .then(restSnapshot => {
						  	rest_snap_data = restSnapshot.data();
							return console.log('Restaurant Snapshot',rest_snap_data);	
					 }).catch(restError => console.log('Users restaurant error', restError));	
					
					if(rest_snap_data.fax){
						const faxNumber = rest_snap_data.fax;
						if(faxNumber && pdfCount === 0){

									const projectId = admin.instanceId().app.options.projectId;
									const Storage = require('@google-cloud/storage');

									const PDFDocument = require ('pdfkit');
									const doc = new PDFDocument;
									const fs = require('fs');
									const uploadTo = '/restaurantOrders/restOrder_FAXCheck.pdf';	

									const gcs = new Storage({
									  projectId: projectId,
									  keyFilename: './credentials.json'
									});
									const bucket = gcs.bucket(bucketName);
									const file = bucket.file(uploadTo);
									//const bucketFileStream = file.createWriteStream();


									var handlebars = require('handlebars');
									var readHTMLFile = function(path, callback) {
									fs.readFile(path, {encoding: 'utf-8'}, function (err, html) {
											if (err) {
												throw err;
												//callback(err);
											}
											else {
												return callback(null, html);
											}
										});
									};
									var pdf = require('html-pdf');

									readHTMLFile('./updated_receipt.html', function(err, html) {
										if (err) {
											console.log("Error in read html file",err);
										}

										var template = handlebars.compile(html);
										var replacements = {
											 customer_name: cust_snap_data.name,
											 order_id: order_id_show,
											 order_time: order_date,
											 order_subtotal: order_subtotal,
											 order_tax: order_tax,
											 order_tip: order_tip,
											 order_total: order_total,
											 products:products,
											 customer_mail: cust_snap_data.emailID,
											 customer_phone: cust_snap_data.mobileNumber,
											 customer_id: customer_id,	
											 order_eaten : "Eaten",
											 special_instructions : special_instructions,
											 customisation : customisation 	
										};

										console.log("Replacement",replacements);
										var htmlToSend = template(replacements);
										//console.log("Html",htmlToSend);											
										//pdf.create(htmlToSend).toStream(function(err, stream) {

										/*var buffers = [];
										let p = new Promise((resolve, reject) => {
											doc.on("end", function() {
											  resolve(buffers);
											});

											doc.on("error", function () {
											  reject(console.log("Finished error"));
											});
										});

										doc.pipe(bucketFileStream);
										doc.text(htmlToSend, {
											width: 410,
											align: 'left'
										});
										doc.end();*/

										/*var options = { 
											  format: 'A4', 
											  border: { top: "0.5cm", right: "0.5cm", bottom: "0.5cm", left: "0.5cm" }, 
											  timeout: 30000 
										};

										pdf.create(htmlToSend, options).toStream(function(err, stream) {	
											if(err){
												console.log("Error in toStream",err);
											}
											stream.pipe(file.createWriteStream());
										});

										//return p.then(function(buffers) {
											return file.getSignedUrl({
											  action: 'read',
											  expires: '03-09-2491'
											}).then(signedUrls => {
											  console.log("PDF Signed",signedUrls[0]);  
											  const pdfUrl = signedUrls[0];
												console.log("IN if condition ");                                            
												//const pdfUrl =  getPdfUrl(rest_id,orderID);
												console.log("PDF Url",pdfUrl);
												if((inArray(['9325012281','9009241741','8983633095','9923673679','9922238594','9649478800','9823057870','9582211120','7058429017'], rest_snap_data.fax))){
												//faxNumber = "+91"+rest_snap_data.fax;	
											}else{
												//faxNumber = "+1"+rest_snap_data.fax;
											}
												//to: faxNumber,
												return client.fax.faxes.create({
													mediaUrl: pdfUrl,
													to: '+19082251401',
													from: twilioNumber
												},function(err,fax){
													if(err){
														console.log("Error while sending fax",err);
													}else{
														pdfCount = 1;
														console.log("fax Status",fax.sid);
													}
												});
											});
										//}); 
									});

								return console.log('Restaurant if condition executed successfully');


						}else{
							return console.log("PDF Already generated");
						}
					}
						return console.log('Email Function executed successfully'); 
				}  
				return console.log('Email function executed successfully');	
		  	}).catch(error => console.log('Couldnt send email notification', error));

	return Promise.all(promises);

});*/

exports.orderEmail = functions.firestore
	.document('/Root/Development/Orders/Orders_Details/Order_Details/{orderId}')
    .onCreate((snap, context) => {
			
			const orderID = context.params.orderId;
			var order_detail_data = snap.data();
	
			const promises = [];
			var order_data = [];
			var rest_id = '';
			var customer_id = '';
			var products = [];
	        var order_subtotal = 0;
			var order_tax = 0;
			var order_tip = 0;
			var order_total = 0;
			var order_date = 0;
			var order_eaten = '';
			var order_taken = '';
			var order_delivery = '';
			var order_id_show = '';
			var special_instructions = '';
			var customisation = [];
	
			//var query = db.collection('Root').doc('Development').collection('Orders').doc('Orders_Menus').collection('Order_Menus').where('order_id', '==', orderID)
			var query = db.collection('Root').doc('Development').collection('Orders').doc('Orders_Details').collection('Order_Details').doc(orderID).collection('Order_Menus')
			  .get()
			  .then(snapshot => {
				snapshot.forEach(order => {
					order_data = order.data();
					
					if(order_detail_data.restaurantID){
						rest_id = order_detail_data.restaurantID;
						customer_id = order_detail_data.userID;
						products.push({product_name:order_data.name,product_count:order_data.count,product_price:order_data.price});
						special_instructions = order_data.instruction;
						customisation.push(order_data.selectedCustomizedLabel);
						//console.log('Customization value',order_data.selectedCustomizedLabel);
						console.log('Customization',customisation);
					}
				});
				  
				if(rest_id !== '' && customer_id !== ''){
					/*db.collection('Root').doc('Development').collection('Orders').doc('Orders_Details').collection('Order_Details').doc(orderID)
						  .get()
						  .then(orderdetails => {
							var order_detail_data = orderdetails.data();*/
							order_subtotal = order_detail_data.subTotal;
							order_tax = order_detail_data.tax;
							order_tip = order_detail_data.tip;
							order_total = order_detail_data.grandTotal;
							order_date = order_detail_data.order_date;
							order_id_show = order_detail_data.order_id_show
							//order_id_show = "Chan1536582038768"	;
						
							/*return console.log('Order Details',order_detail_data); 
					}).catch(error => console.log('Couldnt get order details', error));	*/
					
					promises.push(db.collection('Root').doc('Development').collection('Users_Restaurants').doc(rest_id)
						  .get()
						  .then(restSnapshot => {
							  var rest_snap_data = restSnapshot.data();
							  console.log("REst Email data",rest_snap_data);

								if(rest_snap_data.email){
									db.collection('Root').doc('Development').collection('Users_Consumers').doc(customer_id)
									  .get()
									  .then(custSnapshot => {
											var cust_snap_data = custSnapshot.data();
											console.log("Customer Data send sms", cust_snap_data);
											console.log('Email ID',rest_snap_data.email);

											var handlebars = require('handlebars');
											var fs = require('fs');

											var readHTMLFile = function(path, callback) {
											fs.readFile(path, {encoding: 'utf-8'}, function (err, html) {
													if (err) {
														throw err;
														//callback(err);
													}
													else {
														return callback(null, html);
													}
												});
											};

											let transporter = nodemailer.createTransport({
												host: 'smtp.gmail.com',
												port: 587,
												secure: false,
												requireTLS: true,
												auth: {
													user: 'srokade@webcubator.co',
													pass: 'Sop56an62'
												}
											});

											//readHTMLFile('./receipt.html', function(err, html) {
											readHTMLFile('./updated_receipt.html', function(err, html) {
												if (err) {
													console.log("Error in read html file",err);
												}

												var template = handlebars.compile(html);
												var replacements = {
													 customer_name: cust_snap_data.name,
													 order_id: order_id_show,
													 order_time: order_date,
													 order_subtotal: order_subtotal,
													 order_tax: order_tax,
													 order_tip: order_tip,
													 order_total: order_total,
													 products:products,
													 customer_mail: cust_snap_data.emailID,
													 customer_phone: cust_snap_data.mobileNumber,
													 customer_id: customer_id,	
													 order_eaten : "Eaten",
													 special_instructions : special_instructions,
													 customisation : customisation 	
												};
												var htmlToSend = template(replacements);
												console.log("Html",htmlToSend);											

												let mailOptions = {
													from: '"Food APP Restaurant" <pagrawal@webcubator.co>', // sender address
													to: 'srokade@webcubator.co,schougule@hurekatek.com', // list of receivers
													subject: 'New Order has been generated '+order_id_show, // Subject line
													text: 'You have received a new order from '+cust_snap_data.name+'. Please check FoodKonnect app to check & confirm the order.',
													html: htmlToSend
												};

												transporter.sendMail(mailOptions, (error, info) => {
													if (error) {
														console.log("Error in mail",error);
														return callback(error);
													}
													console.log('Message sent: %s', info.messageId);
												});
											});

										return console.log('Restaurant email executed successfully');
									}).catch(error => console.log('Couldnt send SMS', error));

								}
								return console.log('Email Function executed successfully'); 
						  })
						);
					}  
				return console.log('Email function executed successfully');	
		  	}).catch(error => console.log('Couldnt send email notification', error));
        	
	
				
		return Promise.all(promises);
});

exports.orderEmailLive = functions.firestore
	.document('/Root/Live/Orders/Orders_Details/Order_Details/{orderId}')
    .onCreate((snap, context) => {
			
			const orderID = context.params.orderId;
			var order_detail_data = snap.data();
	
			const promises = [];
			var order_data = [];
			var rest_id = '';
			var customer_id = '';
			var products = [];
	        var order_subtotal = 0;
			var order_tax = 0;
			var order_tip = 0;
			var order_total = 0;
			var order_date = 0;
			var order_eaten = '';
			var order_taken = '';
			var order_delivery = '';
			var order_id_show = '';
			var special_instructions = '';
			var customisation = [];
	
			//var query = db.collection('Root').doc('Live').collection('Orders').doc('Orders_Menus').collection('Order_Menus').where('order_id', '==', orderID)
			var query = db.collection('Root').doc('Live').collection('Orders').doc('Orders_Details').collection('Order_Details').doc(orderID).collection('Order_Menus')
			  .get()
			  .then(snapshot => {
				snapshot.forEach(order => {
					order_data = order.data();
					
					if(order_detail_data.restaurantID){
						rest_id = order_detail_data.restaurantID;
						customer_id = order_detail_data.userID;
						products.push({product_name:order_data.name,product_count:order_data.count,product_price:order_data.price});
						special_instructions = order_data.instruction;
						customisation.push(order_data.selectedCustomizedLabel);
						//console.log('Customization value',order_data.selectedCustomizedLabel);
						console.log('Customization',customisation);
					}
				});
				  
				if(rest_id !== '' && customer_id !== ''){
					/*db.collection('Root').doc('Live').collection('Orders').doc('Orders_Details').collection('Order_Details').doc(orderID)
						  .get()
						  .then(orderdetails => {
							var order_detail_data = orderdetails.data();*/
							order_subtotal = order_detail_data.subTotal;
							order_tax = order_detail_data.tax;
							order_tip = order_detail_data.tip;
							order_total = order_detail_data.grandTotal;
							order_date = order_detail_data.order_date;
							order_id_show = order_detail_data.order_id_show
							//order_id_show = "Chan1536582038768"	;
						
							/*return console.log('Order Details',order_detail_data); 
					}).catch(error => console.log('Couldnt get order details', error));	*/
					
					promises.push(db.collection('Root').doc('Live').collection('Users_Restaurants').doc(rest_id)
						  .get()
						  .then(restSnapshot => {
							  var rest_snap_data = restSnapshot.data();
							  console.log("REst Email data",rest_snap_data);

								if(rest_snap_data.email){
									db.collection('Root').doc('Live').collection('Users_Consumers').doc(customer_id)
									  .get()
									  .then(custSnapshot => {
											var cust_snap_data = custSnapshot.data();
											console.log("Customer Data send sms", cust_snap_data);
											console.log('Email ID',rest_snap_data.email);

											var handlebars = require('handlebars');
											var fs = require('fs');

											var readHTMLFile = function(path, callback) {
											fs.readFile(path, {encoding: 'utf-8'}, function (err, html) {
													if (err) {
														throw err;
														//callback(err);
													}
													else {
														return callback(null, html);
													}
												});
											};

											let transporter = nodemailer.createTransport({
												host: 'smtp.gmail.com',
												port: 587,
												secure: false,
												requireTLS: true,
												auth: {
													user: 'srokade@webcubator.co',
													pass: 'Sop56an62'
												}
											});

											//readHTMLFile('./receipt.html', function(err, html) {
											readHTMLFile('./updated_receipt.html', function(err, html) {
												if (err) {
													console.log("Error in read html file",err);
												}

												var template = handlebars.compile(html);
												var replacements = {
													 customer_name: cust_snap_data.name,
													 order_id: order_id_show,
													 order_time: order_date,
													 order_subtotal: order_subtotal,
													 order_tax: order_tax,
													 order_tip: order_tip,
													 order_total: order_total,
													 products:products,
													 customer_mail: cust_snap_data.emailID,
													 customer_phone: cust_snap_data.mobileNumber,
													 customer_id: customer_id,	
													 order_eaten : "Eaten",
													 special_instructions : special_instructions,
													 customisation : customisation 	
												};
												var htmlToSend = template(replacements);
												console.log("Html",htmlToSend);											

												let mailOptions = {
													from: '"Food APP Restaurant" <pagrawal@webcubator.co>', // sender address
													to: 'srokade@webcubator.co,schougule@hurekatek.com', // list of receivers
													subject: 'New Order has been generated '+order_id_show, // Subject line
													text: 'You have received a new order from '+cust_snap_data.name+'. Please check FoodKonnect app to check & confirm the order.',
													html: htmlToSend
												};

												transporter.sendMail(mailOptions, (error, info) => {
													if (error) {
														console.log("Error in mail",error);
														return callback(error);
													}
													console.log('Message sent: %s', info.messageId);
												});
											});

										return console.log('Restaurant email executed successfully');
									}).catch(error => console.log('Couldnt send SMS', error));

								}
								return console.log('Email Function executed successfully'); 
						  })
						);
					}  
				return console.log('Email function executed successfully');	
		  	}).catch(error => console.log('Couldnt send email notification', error));
        	
	
				
		return Promise.all(promises);
});

exports.orderStatusUpdate = functions.firestore.document('/Root/Development/Orders/Orders_Details/Order_Details/{orderId}').onWrite(
    (change,context) => {
      	const orderKey = context.params.orderId;

		const afterVal = change.after.data();
		const userID = afterVal.userID;
		
		//console.log('AfterVal',afterVal);
		const promises = [];
					//case "0":
				//Status = "Pending";
				//break;
	if(afterVal.orderStatus !== "0"){
		//console.log("In If",afterVal.orderStatus);
		switch(afterVal.orderStatus) {
			case "1":
				Status = "Order confirmation";
				
				//console.log("Confirm Order",afterVal.transactionID);
				
				if(afterVal.transactionID){
					promises.push(gateway.transaction.submitForSettlement(afterVal.transactionID, function (err, result) {
							  if(result){
									  if (result.success) {
										var settledTransaction = result.transaction;
										console.log("Payment Submit for settlement",settledTransaction);  
									  } else {
										console.log("Submit error",result.errors);
									  }
							  }else {
								console.log("Submit Error",err);
							  } 
						})
				  	);
				}	
				break;
			case "2":
				Status = "In kitchen";
				break;
			case "3":
				Status = "Order ready";
				break;
			case "4":
				Status = "Order Closed";
				break;
			case "5":
				Status = "Rejected unable to serve at this time";
				if(afterVal.transactionID){
					promises.push(gateway.transaction.void(afterVal.transactionID, function (err, result) {
							if(result){
							  if (result.success) {
								var settledTransaction = result.transaction;
								console.log("Payment Voided",result.success);  
							  } else {
								console.log("Submit error",result.message);
							  }
							} else {
								console.log("Submit Error",err);
							}	
						})
				  	);
				}
				break;
			default:
				Status = "Pending";
		}
		
		//console.log('User ID',userID);
		

		//promises.push(db.collection('Root').doc('Development').collection('Users').doc('Consumer').collection(userID).doc('tokenFCM').collection("IOS")
		promises.push(db.collection('Root').doc('Development').collection('Users_Consumers').doc(userID).collection(userID).doc('tokenFCM').collection("IOS")			  
			  .get()
			  .then(fcmSnapshot => {
				fcmSnapshot.forEach(fcmChild => {
						var fcmtoken = fcmChild.data();
						var fcmKey = fcmChild.id;
						//console.log("FCM Key",fcmKey);
						const payload = {
						  notification: {
								"title" : "Order Status Update",
								"body" : "Current order status: "+ Status
							},
						  data : {
							"orderId" : orderKey
						  }
						};

						//console.log("IOS Payload", payload);

						admin.messaging().sendToDevice(fcmKey, payload);	
				});
			return console.log('Function executed successfully here');  
		  })
		  );


		//promises.push(db.collection('Root').doc('Development').collection('Users').doc('Consumer').collection(userID).doc('User_Profile')
		promises.push(db.collection('Root').doc('Development').collection('Users_Consumers').doc(userID)			  
						  .get()
						  .then(customSnapshot => {
							  var custom_data = customSnapshot.data();
							  //console.log("User data",custom_data);
								if(custom_data.mobileNumber){
									const ordStatus  = Status;

									//console.log("In Array check",custom_data.mobileNumber);
									var phoneNumber = '';
									if(inArray(['9325012281','9009241741','8983633095','9923673679','9922238594','9649478800','9823057870','9582211120','7058429017'], custom_data.mobileNumber)){
										phoneNumber = "+91"+custom_data.mobileNumber;
										//console.log('Consumer phone',phoneNumber);	
									}else{
										phoneNumber = "+1"+custom_data.mobileNumber;
										//console.log('Else Consumer phone',phoneNumber);	
									}

									if(phoneNumber){
										//console.log("Order status",ordStatus);	
										if ( !validE164(phoneNumber) ) {
											throw new Error('number must be E164 format!')
										}

										const textMessage = {
											body: 'Current order status: '+ ordStatus,
											to: phoneNumber,  // Text to this number
											from: twilioNumber // From a valid Twilio number
										}
										//console.log('Consumer SMS',textMessage);
										client.messages.create(textMessage);
									}
								}
							 return console.log('Function executed successfully'); 
						  }) 
		);
		return Promise.all(promises);
	}else{
		//console.log("In Else",afterVal.orderStatus);
		return console.log('No Order updated.'); 
	}	
});

exports.orderStatusUpdateLive = functions.firestore.document('/Root/Live/Orders/Orders_Details/Order_Details/{orderId}').onWrite(
    (change,context) => {
      	const orderKey = context.params.orderId;

		const afterVal = change.after.data();
		const userID = afterVal.userID;
		
		//console.log('AfterVal',afterVal);
		const promises = [];
					//case "0":
				//Status = "Pending";
				//break;
	if(afterVal.orderStatus !== "0"){
		//console.log("In If",afterVal.orderStatus);
		switch(afterVal.orderStatus) {
			case "1":
				Status = "Order confirmation";
				
				//console.log("Confirm Order",afterVal.transactionID);
				
				if(afterVal.transactionID){
					promises.push(gateway.transaction.submitForSettlement(afterVal.transactionID, function (err, result) {
							  if(result){
									  if (result.success) {
										var settledTransaction = result.transaction;
										console.log("Payment Submit for settlement",settledTransaction);  
									  } else {
										console.log("Submit error",result.errors);
									  }
							  }else {
								console.log("Submit Error",err);
							  } 
						})
				  	);
				}	
				break;
			case "2":
				Status = "In kitchen";
				break;
			case "3":
				Status = "Order ready";
				break;
			case "4":
				Status = "Order Closed";
				break;
			case "5":
				Status = "Rejected unable to serve at this time";
				if(afterVal.transactionID){
					promises.push(gateway.transaction.void(afterVal.transactionID, function (err, result) {
							if(result){
							  if (result.success) {
								var settledTransaction = result.transaction;
								console.log("Payment Voided",result.success);  
							  } else {
								console.log("Submit error",result.message);
							  }
							} else {
								console.log("Submit Error",err);
							}	
						})
				  	);
				}
				break;
			default:
				Status = "Pending";
		}
		
		//console.log('User ID',userID);
		

		//promises.push(db.collection('Root').doc('Development').collection('Users').doc('Consumer').collection(userID).doc('tokenFCM').collection("IOS")
		promises.push(db.collection('Root').doc('Live').collection('Users_Consumers').doc(userID).collection(userID).doc('tokenFCM').collection("IOS")			  
			  .get()
			  .then(fcmSnapshot => {
				fcmSnapshot.forEach(fcmChild => {
						var fcmtoken = fcmChild.data();
						var fcmKey = fcmChild.id;
						//console.log("FCM Key",fcmKey);
						const payload = {
						  notification: {
								"title" : "Order Status Update",
								"body" : "Current order status: "+ Status
							},
						  data : {
							"orderId" : orderKey
						  }
						};

						//console.log("IOS Payload", payload);

						admin.messaging().sendToDevice(fcmKey, payload);	
				});
			return console.log('Function executed successfully here');  
		  })
		  );


		//promises.push(db.collection('Root').doc('Development').collection('Users').doc('Consumer').collection(userID).doc('User_Profile')
		promises.push(db.collection('Root').doc('Live').collection('Users_Consumers').doc(userID)			  
						  .get()
						  .then(customSnapshot => {
							  var custom_data = customSnapshot.data();
							  //console.log("User data",custom_data);
								if(custom_data.mobileNumber){
									const ordStatus  = Status;

									//console.log("In Array check",custom_data.mobileNumber);
									var phoneNumber = '';
									if(inArray(['9325012281','9009241741','8983633095','9923673679','9922238594','9649478800','9823057870','9582211120','7058429017'], custom_data.mobileNumber)){
										phoneNumber = "+91"+custom_data.mobileNumber;
										//console.log('Consumer phone',phoneNumber);	
									}else{
										phoneNumber = "+1"+custom_data.mobileNumber;
										//console.log('Else Consumer phone',phoneNumber);	
									}

									if(phoneNumber){
										//console.log("Order status",ordStatus);	
										if ( !validE164(phoneNumber) ) {
											throw new Error('number must be E164 format!')
										}

										const textMessage = {
											body: 'Current order status: '+ ordStatus,
											to: phoneNumber,  // Text to this number
											from: twilioNumber // From a valid Twilio number
										}
										//console.log('Consumer SMS',textMessage);
										client.messages.create(textMessage);
									}
								}
							 return console.log('Function executed successfully'); 
						  }) 
		);
		return Promise.all(promises);
	}else{
		//console.log("In Else",afterVal.orderStatus);
		return console.log('No Order updated.'); 
	}	
});

function validE164(num) {
    return /^\+?[1-9]\d{1,14}$/.test(num)
}


exports.addMessage = functions.https.onRequest((req, res) => {
  	//console.log("Request Body",req.body);
	var orderID = req.query.orderId;
	console.log("Order ID",orderID);
	
	const twiml = new VoiceResponse();

	  /** helper function to set up a <Gather> */
	  function gather() {
		const gatherNode = twiml.gather({ numDigits: 1 });
		gatherNode.say('You have got the order. Press 1, to confirm the order. Press 2, to reject the order.');

		// If the user doesn't enter input, loop
		twiml.redirect('/addMessage');
	  }
	
	var d1 = new Date ();
    d2 = new Date ( d1 );
	d2.setMinutes ( d1.getMinutes() + 30 );
	console.log("d2",d2.getTime());
	  // If the user entered digits, process their request
	  if (req.body.Digits) {
		switch (req.body.Digits) {
		  case '1':
			twiml.say('You confirmed the Order. Thank you!');			
				
			db.collection('Root').doc('Development').collection('Orders').doc('Orders_Details').collection('Order_Details').doc(orderID).update({'orderStatus': '1'});				
			db.collection('Root').doc('Development').collection('Orders').doc('Orders_Details').collection('Order_Details').doc(orderID).update({'pickupTime': d2.getTime()});					
			break;
		  case '2':
			twiml.say('You rejected the order. Thank you!');
				
			db.collection('Root').doc('Development').collection('Orders').doc('Orders_Details').collection('Order_Details').doc(orderID).update({'orderStatus': '5'});				
			break;
		  default:
			twiml.say("Sorry, I don't understand that choice, ");
			gather();
			break;
		}
	  } else {
		// If no input was sent, use the <Gather> verb to collect user input
		gather();
	  }

	  // Render the response as XML in reply to the webhook request
	  res.type('text/xml');
	  res.send(twiml.toString());
	return console.log(res.send);
});

exports.addMessageLive = functions.https.onRequest((req, res) => {
  	//console.log("Request Body",req.body);
	var orderID = req.query.orderId;
	console.log("Order ID",orderID);
	
	const twiml = new VoiceResponse();

	  /** helper function to set up a <Gather> */
	  function gather() {
		const gatherNode = twiml.gather({ numDigits: 1 });
		gatherNode.say('You have got the order. Press 1, to confirm the order. Press 2, to reject the order.');

		// If the user doesn't enter input, loop
		twiml.redirect('/addMessageLive');
	  }
	
	var d1 = new Date ();
    d2 = new Date ( d1 );
	d2.setMinutes ( d1.getMinutes() + 30 );
	console.log("d2",d2.getTime());
	  // If the user entered digits, process their request
	  if (req.body.Digits) {
		switch (req.body.Digits) {
		  case '1':
			twiml.say('You confirmed the Order. Thank you!');			
				
			db.collection('Root').doc('Live').collection('Orders').doc('Orders_Details').collection('Order_Details').doc(orderID).update({'orderStatus': '1'});				
			db.collection('Root').doc('Live').collection('Orders').doc('Orders_Details').collection('Order_Details').doc(orderID).update({'pickupTime': d2.getTime()});					
			break;
		  case '2':
			twiml.say('You rejected the order. Thank you!');
				
			db.collection('Root').doc('Live').collection('Orders').doc('Orders_Details').collection('Order_Details').doc(orderID).update({'orderStatus': '5'});				
			break;
		  default:
			twiml.say("Sorry, I don't understand that choice, ");
			gather();
			break;
		}
	  } else {
		// If no input was sent, use the <Gather> verb to collect user input
		gather();
	  }

	  // Render the response as XML in reply to the webhook request
	  res.type('text/xml');
	  res.send(twiml.toString());
	return console.log(res.send);
});

// The name for the new bucket
const bucketName = 'foodkonnect-65adf.appspot.com';


exports.bucketCreate = functions.https.onRequest((req, res) => {
	const projectId = admin.instanceId().app.options.projectId;
	const Storage = require('@google-cloud/storage');
	const pdf = require('html-pdf');
	const path = require('path');
	const os = require('os');
	const handlebars = require('handlebars');
	const fs = require('fs');
	const tmp = require('tmp');
	
	const gcs = new Storage({
	  projectId: projectId,
	  keyFilename: './credentials.json'
	});
	const bucket = gcs.bucket(bucketName);
	
	const user = {
	 "date": new Date().toISOString(),
	 "name" : "Swapnil",
    };
    
	const options = {
	 "format": 'A4',
	 "orientation": "portrait"
    };
	
	//const localTemplate = path.join(os.tmpdir(), 'restaurantOrders/restaurant_order_template.html');
	// Create a reference from a Google Cloud Storage URI
	//var localTemplate = storage.refFromURL('gs://bucket/images/stars.jpg')
	
	//const localTemplate = '/restaurantOrders/restaurant_order_template.html';
  	//const localPDFFile = tmp.tmpNameSync();
	
	 //bucket.file(localTemplate).download({ destination: localPDFFile }).then(() => {
		//console.log("template downloaded locally");
		const source = './Example.html';
		const html = handlebars.compile(source)(user);
		
		console.log("Source", source);
		console.log("template compiled with user data", html);
		
		 //const html = "Test this data";
		 
		pdf.create(html, options).toFile("./Example.pdf", function(err, res) {
			  if (err){
				console.log(err);
				return console.log("PDF creation error");
			  }
			  console.log("pdf created locally",res);

			  return bucket.upload("./Example.pdf", { destination: 'restaurantOrders/' +user.name + '.pdf', metadata: { contentType: 'application/pdf'}}).then(() => {
				return console.log("PDF created and uploaded!");
			  }).catch(error => {
				console.error(error);
				return console.log("PDF created and uploaded!");
			  });
	    });
		 //return response.send("PDF uploaded!");
	 /* }).catch(pdfError => {
		console.error(pdfError);
		return response.send("Error while downloading pdf!",pdfError);
	  });*/
	
	/*bucket.upload("./credentials.json", {
	  destination: "restaurantOrders/file.json"
	}, function(err, file) {
	  if (!err) {
		console.log("File",file);
	  }else{
	  	console.log("Err",err);
	  }
	});*/
});	


exports.bucketCreateLive = functions.https.onRequest((req, res) => {
	const projectId = admin.instanceId().app.options.projectId;
	const Storage = require('@google-cloud/storage');
	const pdf = require('html-pdf');
	const path = require('path');
	const os = require('os');
	const handlebars = require('handlebars');
	const fs = require('fs');
	const tmp = require('tmp');
	
	const gcs = new Storage({
	  projectId: projectId,
	  keyFilename: './credentials.json'
	});
	const bucket = gcs.bucket(bucketName);
	
	const user = {
	 "date": new Date().toISOString(),
	 "name" : "Swapnil",
    };
    
	const options = {
	 "format": 'A4',
	 "orientation": "portrait"
    };
	
	//const localTemplate = path.join(os.tmpdir(), 'restaurantOrders/restaurant_order_template.html');
	// Create a reference from a Google Cloud Storage URI
	//var localTemplate = storage.refFromURL('gs://bucket/images/stars.jpg')
	
	//const localTemplate = '/restaurantOrders/restaurant_order_template.html';
  	//const localPDFFile = tmp.tmpNameSync();
	
	 //bucket.file(localTemplate).download({ destination: localPDFFile }).then(() => {
		//console.log("template downloaded locally");
		const source = './Example.html';
		const html = handlebars.compile(source)(user);
		
		console.log("Source", source);
		console.log("template compiled with user data", html);
		
		 //const html = "Test this data";
		 
		pdf.create(html, options).toFile("./Example.pdf", function(err, res) {
			  if (err){
				console.log(err);
				return console.log("PDF creation error");
			  }
			  console.log("pdf created locally",res);

			  return bucket.upload("./Example.pdf", { destination: 'restaurantOrders/' +user.name + '.pdf', metadata: { contentType: 'application/pdf'}}).then(() => {
				return console.log("PDF created and uploaded!");
			  }).catch(error => {
				console.error(error);
				return console.log("PDF created and uploaded!");
			  });
	    });
		 //return response.send("PDF uploaded!");
	 /* }).catch(pdfError => {
		console.error(pdfError);
		return response.send("Error while downloading pdf!",pdfError);
	  });*/
	
	/*bucket.upload("./credentials.json", {
	  destination: "restaurantOrders/file.json"
	}, function(err, file) {
	  if (!err) {
		console.log("File",file);
	  }else{
	  	console.log("Err",err);
	  }
	});*/
});	

exports.restOrderCreate = functions.https.onRequest((req, res) => {
	const projectId = admin.instanceId().app.options.projectId;
	const Storage = require('@google-cloud/storage');
	
	const PDFDocument = require ('pdfkit');
	const doc = new PDFDocument;
	const fs = require('fs');
	
	const filePath = '/tmp/new_output_here.pdf';
	const uploadTo = '/restaurantOrders/new_output_here11.pdf';
	
	const gcs = new Storage({
	  projectId: projectId,
	  keyFilename: './credentials.json'
	});
	const bucket = gcs.bucket(bucketName);
	
	const stream = fs.createWriteStream(filePath,'binary');

	stream.once('open', (fd) => {
		stream.write("First line\n");
		stream.write("Second line\n");

		// Important to close the stream when you're ready
		stream.end();
	});
	/*const pdfText = "Some sample text here";
	doc.pipe(fs.createWriteStream(filePath,'binary'));
	
	doc.text(pdfText);
	
	doc.end();*/
	
	//console.log("Doc Data",doc);
	/*stream.on('finish', function() {
	  iframe.src = stream.toBlobURL('application/pdf');
	});*/
	
	  stream.on('finish', function () {
		// call the callback function or in my case resolve the Promise.
			bucket.upload(filePath, { destination: uploadTo}).then(() => {
				const thumbFile = bucket.file(uploadTo);
				
				const storageURL = thumbFile.getSignedUrl();
				
				return console.log("PDF created and uploaded here!",storageURL);
			}).catch(error => {
				console.error(error);
				return console.log("PDF created Error!");
			});
	  });
	
	/*var wstream = fs.createWriteStream('./output111.txt');
	wstream.write('Hello world!\n');
	wstream.write('Another line\n');
	wstream.end();*/
});

exports.restOrderCreateLive = functions.https.onRequest((req, res) => {
	const projectId = admin.instanceId().app.options.projectId;
	const Storage = require('@google-cloud/storage');
	
	const PDFDocument = require ('pdfkit');
	const doc = new PDFDocument;
	const fs = require('fs');
	
	const filePath = '/tmp/new_output_here.pdf';
	const uploadTo = '/restaurantOrders/new_output_here11.pdf';
	
	const gcs = new Storage({
	  projectId: projectId,
	  keyFilename: './credentials.json'
	});
	const bucket = gcs.bucket(bucketName);
	
	const stream = fs.createWriteStream(filePath,'binary');

	stream.once('open', (fd) => {
		stream.write("First line\n");
		stream.write("Second line\n");

		// Important to close the stream when you're ready
		stream.end();
	});
	/*const pdfText = "Some sample text here";
	doc.pipe(fs.createWriteStream(filePath,'binary'));
	
	doc.text(pdfText);
	
	doc.end();*/
	
	//console.log("Doc Data",doc);
	/*stream.on('finish', function() {
	  iframe.src = stream.toBlobURL('application/pdf');
	});*/
	
	  stream.on('finish', function () {
		// call the callback function or in my case resolve the Promise.
			bucket.upload(filePath, { destination: uploadTo}).then(() => {
				const thumbFile = bucket.file(uploadTo);
				
				const storageURL = thumbFile.getSignedUrl();
				
				return console.log("PDF created and uploaded here!",storageURL);
			}).catch(error => {
				console.error(error);
				return console.log("PDF created Error!");
			});
	  });
	
	/*var wstream = fs.createWriteStream('./output111.txt');
	wstream.write('Hello world!\n');
	wstream.write('Another line\n');
	wstream.end();*/
});

exports.pdfCheck = functions.https.onRequest((req, res) => {
	const projectId = admin.instanceId().app.options.projectId;
	const Storage = require('@google-cloud/storage');
	
	const PDFDocument = require ('pdfkit');
	const doc = new PDFDocument;
	const fs = require('fs');
	
	const filePath = '/tmp/my.pdf';
	const uploadTo = '/restaurantOrders/my_new_pdf.pdf';
	console.log("PDF projectId",projectId);
	const gcs = new Storage({
	  projectId: projectId,
	  keyFilename: './credentials.json'
	});
	const bucket = gcs.bucket(bucketName);
	const file = bucket.file(uploadTo);
	console.log("PDF bucket",bucket);
	const bucketFileStream = file.createWriteStream();

	var buffers = [];
	let p = new Promise((resolve, reject) => {
		doc.on("end", function() {
		  resolve(buffers);
		});
	
		doc.on("error", function () {
		  reject(console.log("Finished error"));
		});
    });
	
	doc.pipe(bucketFileStream);
	doc.text('My testing content here.', {
		width: 410,
		align: 'left'
	});
	doc.end();
	
	return p.then(function(buffers) {
     	return file.getSignedUrl({
		  action: 'read',
		  expires: '03-09-2491'
		}).then(signedUrls => {
		console.log("PDF Url",signedUrls[0]);	
		  //return signedUrls[0];
			return client.fax.faxes.create({
				mediaUrl: signedUrls[0],
				to: '+19082251401',
				from: twilioNumber
			},function(err,fax){
				if(err){
					console.log("Error while sending fax",err);
				}else{
					pdfCount = 1;
					console.log("fax Status",fax.sid);
				}
			});
		});
  	});
});

exports.pdfCheckLive = functions.https.onRequest((req, res) => {
	const projectId = admin.instanceId().app.options.projectId;
	const Storage = require('@google-cloud/storage');
	
	const PDFDocument = require ('pdfkit');
	const doc = new PDFDocument;
	const fs = require('fs');
	
	const filePath = '/tmp/my.pdf';
	const uploadTo = '/restaurantOrders/my_new_pdf.pdf';
	console.log("PDF projectId",projectId);
	const gcs = new Storage({
	  projectId: projectId,
	  keyFilename: './credentials.json'
	});
	const bucket = gcs.bucket(bucketName);
	const file = bucket.file(uploadTo);
	console.log("PDF bucket",bucket);
	const bucketFileStream = file.createWriteStream();

	var buffers = [];
	let p = new Promise((resolve, reject) => {
		doc.on("end", function() {
		  resolve(buffers);
		});
	
		doc.on("error", function () {
		  reject(console.log("Finished error"));
		});
    });
	
	doc.pipe(bucketFileStream);
	doc.text('My testing content here.', {
		width: 410,
		align: 'left'
	});
	doc.end();
	
	return p.then(function(buffers) {
     	return file.getSignedUrl({
		  action: 'read',
		  expires: '03-09-2491'
		}).then(signedUrls => {
		console.log("PDF Url",signedUrls[0]);	
		  //return signedUrls[0];
			return client.fax.faxes.create({
				mediaUrl: signedUrls[0],
				to: '+19082251401',
				from: twilioNumber
			},function(err,fax){
				if(err){
					console.log("Error while sending fax",err);
				}else{
					pdfCount = 1;
					console.log("fax Status",fax.sid);
				}
			});
		});
  	});
});

function getPdfUrl(restID,orderID){
	const projectId = admin.instanceId().app.options.projectId;
	const Storage = require('@google-cloud/storage');
	
	const PDFDocument = require ('pdfkit');
	const doc = new PDFDocument;
	const fs = require('fs');
	
	const filePath = '/tmp/my.pdf';
	const uploadTo = '/restaurantOrders/'+ restID +'_'+ orderID +'.pdf';
	
	const gcs = new Storage({
	  projectId: projectId,
	  keyFilename: './credentials.json'
	});
	const bucket = gcs.bucket(bucketName);
	const file = bucket.file(uploadTo);
	
	const bucketFileStream = file.createWriteStream();

	var buffers = [];
	let p = new Promise((resolve, reject) => {
		doc.on("end", function() {
		  resolve(buffers);
		});
	
		doc.on("error", function () {
		  reject(console.log("Finished error"));
		});
    });
	
	doc.pipe(bucketFileStream);
	doc.text('My testing content here.', {
		width: 410,
		align: 'left'
	});
	doc.end();
	
	return p.then(function(buffers) {
     	return file.getSignedUrl({
		  action: 'read',
		  expires: '03-09-2491'
		}).then(signedUrls => {
          console.log("PDF",signedUrls[0]);  
		  return signedUrls[0];
		});
  	});

}


 exports.checkout = functions.https.onRequest((request, response) => {

	 var amount = request.body.amount;
	 var braintreeId = request.body.braintreeId;
	 var restaurantID = request.body.restaurantID;
	 var consumerId = request.body.consumerId;
	 var paymentMethodNonce = request.body.paymentMethodNonce;
	 var paymentMethodToken = request.body.paymentMethod_CCToken;
	 var name = request.body.name;
	 var emailid = request.body.emailId;
	 var mobileNumber = request.body.mobileNumber;
	 var card_number = request.body.card_number;
	 var expiration_date = request.body.expiration_date;
	 var cvv = request.body.cvv;
	 var defaultPaymentMethod = request.body.defaultPaymentMethod;

	 if(!(consumerId) || consumerId === ''){
	 	response.json({"status":"error","error": "Consumer ID is missing."});
		response.end(); 
	 }
	 if(!(amount) || amount === ''){
	 	response.json({"status":"error","error": "Amount is missing."});
		response.end(); 
	 }
	 	 
	 if(!(braintreeId)){
				gateway.customer.create({
				  firstName: name,
				  lastName: "",
				  email: emailid,
				  phone: mobileNumber
				}, function (err, result) {
					 if(result){
						braintreeId = result.customer.id;
						 
						if(card_number && card_number !== ''){
							const creditCardParams = {
							  customerId:braintreeId,
							  number: card_number,
							  expirationDate: expiration_date,
							  cvv: cvv
							};

							gateway.creditCard.create(creditCardParams, function (card_err, card_response) {
								if(card_response){
									//response.json({"status":"success","card_token": card_response});
								} else {
									response.json({"status":"error","error": card_err});
								}
							});
						}
						
						 if(paymentMethodToken){
							 if(defaultPaymentMethod && defaultPaymentMethod !== ''){
								/* gateway.paymentMethod.update(paymentMethodToken, {
									  options: {
										makeDefault: true
									  }
									},function(method_error, method_result) {
									  if (method_result) {
										  if(method_result.success){
											response.json({"status":"success","method_response":method_result});
										  }else {
											response.json({"status":"error","error": method_result.message});  	
										  }
											response.end(); 
									  } else {
										response.json({"status":"error","error": method_error.message});
										response.end();  
									  }
								});*/
							 }
								 
							 gateway.transaction.sale({
								amount: amount,
								paymentMethodToken: paymentMethodToken,
								customerId: braintreeId,
								customFields: {
									restaurantid: restaurantID  
								}
							  }, function(tra_error, tra_result) {
								  if (tra_result) {
									  if(tra_result.success){
										response.json({"status":"success","braintreeId": braintreeId,"transactionId":tra_result.transaction.id});
									  }else {
										response.json({"status":"error","error": tra_result.message});  	
									  }
										response.end(); 
								  } else {
									response.json({"status":"error","error": tra_error.message});
									response.end();  
								  }
							});
						 }else{
							 
							if(defaultPaymentMethod && defaultPaymentMethod !== ''){
								/* gateway.paymentMethod.update(paymentMethodNonce, {
									  options: {
										makeDefault: true
									  }
									},function(method_error, method_result) {
									  if (method_result) {
										  if(method_result.success){
											response.json({"status":"success","method_response":method_result});
										  }else {
											response.json({"status":"error","error": method_result.message});  	
										  }
											response.end(); 
									  } else {
										response.json({"status":"error","error": method_error.message});
										response.end();  
									  }
								});*/
							 }
							 
							gateway.transaction.sale({
								amount: amount,
								paymentMethodNonce: paymentMethodNonce,
								customerId: braintreeId,
								customFields: {
									restaurantid: restaurantID  
								}
							  }, function(tra_error, tra_result) {
								  if (tra_result) {
									  if(tra_result.success){
										response.json({"status":"success","braintreeId": braintreeId,"transactionId":tra_result.transaction.id});
									  }else {
										response.json({"status":"error","error": tra_result.message});  	
									  }
										response.end(); 
								  } else {
									response.json({"status":"error","error": tra_error.message});
									response.end();  
								  }
							});
						 }
					 }else{
						response.status(500).send(err);
					 }
				});
			
	 }else{
		 
		 if(card_number && card_number !== ''){
			const creditCardParams = {
			  customerId:braintreeId,
			  number: card_number,
			  expirationDate: expiration_date,
			  cvv: cvv
			};

			gateway.creditCard.create(creditCardParams, function (card_err, card_response) {
				if(card_response){
					//response.json({"status":"success","card_token": card_response});
				} else {
					response.json({"status":"error","error": card_err});
				}
			});
		}		
		 if(paymentMethodToken){
			if(defaultPaymentMethod && defaultPaymentMethod !== ''){
				 /*gateway.paymentMethod.update(paymentMethodToken, {
					  options: {
						makeDefault: true
					  }
					},function(method_error, method_result) {
					  if (method_result) {
						  if(method_result.success){
							response.json({"status":"success","method_response":method_result});
						  }else {
							response.json({"status":"error","error": method_result.message});  	
						  }
							response.end(); 
					  } else {
						response.json({"status":"error","error": method_error.message});
						response.end();  
					  }
				});*/
			 } 
		 	gateway.transaction.sale({
				amount: amount,
				paymentMethodToken: paymentMethodToken,
				customerId: braintreeId,
				customFields: {
					restaurantid: restaurantID  
				}
			  }, function(error, result) {
				  if (result) {
					if(result.success){
						response.json({"status":"success","braintreeId": braintreeId,"transactionId":result.transaction.id});
					  }else {
						response.json({"status":"error","error": result.message});  	
					  }
					response.end(); 
				  } else {
					response.status(500).send(error);
				  }
		  });
		 }else{
		  if(defaultPaymentMethod && defaultPaymentMethod !== ''){
				 /*gateway.paymentMethod.update(paymentMethodNonce, {
					  options: {
						makeDefault: true
					  }
					},function(method_error, method_result) {
					  if (method_result) {
						  if(method_result.success){
							response.json({"status":"success","method_response":method_result});
						  }else {
							response.json({"status":"error","error": method_result.message});  	
						  }
							response.end(); 
					  } else {
						response.json({"status":"error","error": method_error.message});
						response.end();  
					  }
				});*/
			 } 	 
				 
		  gateway.transaction.sale({
			amount: amount,
			paymentMethodNonce: paymentMethodNonce,
			customerId: braintreeId,
			customFields: {
				restaurantid: restaurantID  
			}  
		  }, function(error, result) {
			  if (result) {
				if(result.success){
					response.json({"status":"success","braintreeId": braintreeId,"transactionId":result.transaction.id});
				  }else {
					response.json({"status":"error","error": result.message});  	
				  }
				response.end(); 
			  } else {
				response.status(500).send(error);
			  }
		  });
		 }	 
	 }
 });

exports.checkoutLive = functions.https.onRequest((request, response) => {

	 var amount = request.body.amount;
	 var braintreeId = request.body.braintreeId;
	 var restaurantID = request.body.restaurantID;
	 var consumerId = request.body.consumerId;
	 var paymentMethodNonce = request.body.paymentMethodNonce;
	 var paymentMethodToken = request.body.paymentMethod_CCToken;
	 var name = request.body.name;
	 var emailid = request.body.emailId;
	 var mobileNumber = request.body.mobileNumber;
	 var card_number = request.body.card_number;
	 var expiration_date = request.body.expiration_date;
	 var cvv = request.body.cvv;
	 var defaultPaymentMethod = request.body.defaultPaymentMethod;

	 if(!(consumerId) || consumerId === ''){
	 	response.json({"status":"error","error": "Consumer ID is missing."});
		response.end(); 
	 }
	 if(!(amount) || amount === ''){
	 	response.json({"status":"error","error": "Amount is missing."});
		response.end(); 
	 }
	 	 
	 if(!(braintreeId)){
				gateway.customer.create({
				  firstName: name,
				  lastName: "",
				  email: emailid,
				  phone: mobileNumber
				}, function (err, result) {
					 if(result){
						braintreeId = result.customer.id;
						 
						if(card_number && card_number !== ''){
							const creditCardParams = {
							  customerId:braintreeId,
							  number: card_number,
							  expirationDate: expiration_date,
							  cvv: cvv
							};

							gateway.creditCard.create(creditCardParams, function (card_err, card_response) {
								if(card_response){
									//response.json({"status":"success","card_token": card_response});
								} else {
									response.json({"status":"error","error": card_err});
								}
							});
						}
						
						 if(paymentMethodToken){
							 if(defaultPaymentMethod && defaultPaymentMethod !== ''){
								/* gateway.paymentMethod.update(paymentMethodToken, {
									  options: {
										makeDefault: true
									  }
									},function(method_error, method_result) {
									  if (method_result) {
										  if(method_result.success){
											response.json({"status":"success","method_response":method_result});
										  }else {
											response.json({"status":"error","error": method_result.message});  	
										  }
											response.end(); 
									  } else {
										response.json({"status":"error","error": method_error.message});
										response.end();  
									  }
								});*/
							 }
								 
							 gateway.transaction.sale({
								amount: amount,
								paymentMethodToken: paymentMethodToken,
								customerId: braintreeId,
								customFields: {
									restaurantid: restaurantID  
								}
							  }, function(tra_error, tra_result) {
								  if (tra_result) {
									  if(tra_result.success){
										response.json({"status":"success","braintreeId": braintreeId,"transactionId":tra_result.transaction.id});
									  }else {
										response.json({"status":"error","error": tra_result.message});  	
									  }
										response.end(); 
								  } else {
									response.json({"status":"error","error": tra_error.message});
									response.end();  
								  }
							});
						 }else{
							 
							if(defaultPaymentMethod && defaultPaymentMethod !== ''){
								/* gateway.paymentMethod.update(paymentMethodNonce, {
									  options: {
										makeDefault: true
									  }
									},function(method_error, method_result) {
									  if (method_result) {
										  if(method_result.success){
											response.json({"status":"success","method_response":method_result});
										  }else {
											response.json({"status":"error","error": method_result.message});  	
										  }
											response.end(); 
									  } else {
										response.json({"status":"error","error": method_error.message});
										response.end();  
									  }
								});*/
							 }
							 
							gateway.transaction.sale({
								amount: amount,
								paymentMethodNonce: paymentMethodNonce,
								customerId: braintreeId,
								customFields: {
									restaurantid: restaurantID  
								}
							  }, function(tra_error, tra_result) {
								  if (tra_result) {
									  if(tra_result.success){
										response.json({"status":"success","braintreeId": braintreeId,"transactionId":tra_result.transaction.id});
									  }else {
										response.json({"status":"error","error": tra_result.message});  	
									  }
										response.end(); 
								  } else {
									response.json({"status":"error","error": tra_error.message});
									response.end();  
								  }
							});
						 }
					 }else{
						response.status(500).send(err);
					 }
				});
			
	 }else{
		 
		 if(card_number && card_number !== ''){
			const creditCardParams = {
			  customerId:braintreeId,
			  number: card_number,
			  expirationDate: expiration_date,
			  cvv: cvv
			};

			gateway.creditCard.create(creditCardParams, function (card_err, card_response) {
				if(card_response){
					//response.json({"status":"success","card_token": card_response});
				} else {
					response.json({"status":"error","error": card_err});
				}
			});
		}		
		 if(paymentMethodToken){
			if(defaultPaymentMethod && defaultPaymentMethod !== ''){
				 /*gateway.paymentMethod.update(paymentMethodToken, {
					  options: {
						makeDefault: true
					  }
					},function(method_error, method_result) {
					  if (method_result) {
						  if(method_result.success){
							response.json({"status":"success","method_response":method_result});
						  }else {
							response.json({"status":"error","error": method_result.message});  	
						  }
							response.end(); 
					  } else {
						response.json({"status":"error","error": method_error.message});
						response.end();  
					  }
				});*/
			 } 
		 	gateway.transaction.sale({
				amount: amount,
				paymentMethodToken: paymentMethodToken,
				customerId: braintreeId,
				customFields: {
					restaurantid: restaurantID  
				}
			  }, function(error, result) {
				  if (result) {
					if(result.success){
						response.json({"status":"success","braintreeId": braintreeId,"transactionId":result.transaction.id});
					  }else {
						response.json({"status":"error","error": result.message});  	
					  }
					response.end(); 
				  } else {
					response.status(500).send(error);
				  }
		  });
		 }else{
		  if(defaultPaymentMethod && defaultPaymentMethod !== ''){
				 /*gateway.paymentMethod.update(paymentMethodNonce, {
					  options: {
						makeDefault: true
					  }
					},function(method_error, method_result) {
					  if (method_result) {
						  if(method_result.success){
							response.json({"status":"success","method_response":method_result});
						  }else {
							response.json({"status":"error","error": method_result.message});  	
						  }
							response.end(); 
					  } else {
						response.json({"status":"error","error": method_error.message});
						response.end();  
					  }
				});*/
			 } 	 
				 
		  gateway.transaction.sale({
			amount: amount,
			paymentMethodNonce: paymentMethodNonce,
			customerId: braintreeId,
			customFields: {
				restaurantid: restaurantID  
			}  
		  }, function(error, result) {
			  if (result) {
				if(result.success){
					response.json({"status":"success","braintreeId": braintreeId,"transactionId":result.transaction.id});
				  }else {
					response.json({"status":"error","error": result.message});  	
				  }
				response.end(); 
			  } else {
				response.status(500).send(error);
			  }
		  });
		 }	 
	 }
 });

exports.consumer_cards = functions.https.onRequest((request, response) => {

	 var braintreeId = request.body.braintreeId;
	
	 if(!(braintreeId) || braintreeId === ''){
	 	response.json({"status":"error","error": "Consumer ID is missing."});
		response.end(); 
	 }	
	
	 gateway.customer.find(braintreeId, function(err, customer) {
		if(customer){
			var card_data = [];
			customer.creditCards.forEach(cards => {
				card_data.push({
					"cardType":cards.cardType,
					"cardholderName": cards.cardholderName,
					"expirationMonth":cards.expirationMonth,
					"expirationYear": cards.expirationYear,
					"cardImage":cards.imageUrl,
					"cardNumber":cards.maskedNumber,
					"token":cards.token
				});
			});
			response.json({"status":"success","card_list":card_data});
			response.end();
		}else {
			response.json({"status":"error","error_message":"User not found","error_type":err.name});
			response.end();
		}
	 });
 });

exports.consumer_cardsLive = functions.https.onRequest((request, response) => {

	 var braintreeId = request.body.braintreeId;
	
	 if(!(braintreeId) || braintreeId === ''){
	 	response.json({"status":"error","error": "Consumer ID is missing."});
		response.end(); 
	 }	
	
	 gateway.customer.find(braintreeId, function(err, customer) {
		if(customer){
			var card_data = [];
			customer.creditCards.forEach(cards => {
				card_data.push({
					"cardType":cards.cardType,
					"cardholderName": cards.cardholderName,
					"expirationMonth":cards.expirationMonth,
					"expirationYear": cards.expirationYear,
					"cardImage":cards.imageUrl,
					"cardNumber":cards.maskedNumber,
					"token":cards.token
				});
			});
			response.json({"status":"success","card_list":card_data});
			response.end();
		}else {
			response.json({"status":"error","error_message":"User not found","error_type":err.name});
			response.end();
		}
	 });
 });

exports.restaurant_fax = functions.https.onRequest((request, response) => {
	
	var orderID = request.body.orderID;
	var customer_id = request.body.customerId;
	
	if(!(orderID) || orderID === ''){
	 	response.json({"status":"error","error": "Order ID is missing."});
		response.end(); 
	 }
	
	
	const promises = [];
	var order_data = [];
	var rest_id = '';
	var products = [];
	var order_eaten = '';
	var order_taken = '';
	var order_delivery = '';
	var order_id_show = '';
	var special_instructions = '';
	//var customisation = [];
	//var rest_snap_data;
	//var cust_snap_data;
	
	//var OrderMenuref = db.collection('Root').doc('Development').collection('Orders').doc('Orders_Menus').collection('Order_Menus').where('order_id', '==', orderID).get()
	var OrderMenuref = db.collection('Root').doc('Development').collection('Orders').doc('Orders_Details').collection('Order_Details').doc(orderID).collection('Order_Menus').get()
			  .then(ordSnapshot => {
				ordSnapshot.forEach(order => {
					order_data = order.data();
					products.push({product_name:order_data.name,product_count:order_data.count,product_price:order_data.price});
				});
				  return order_data;
			  }).catch(menuError => console.log('Couldnt get Order menu details', menuError));

								 
	var orderDetailRef = db.collection('Root').doc('Development').collection('Orders').doc('Orders_Details').collection('Order_Details').doc(orderID).get()
						  .then(orderdetails => {
							return order_detail_data = orderdetails.data();
						}).catch(orderError => console.log('Couldnt get order details', orderError));
	
	var consumerRef = db.collection('Root').doc('Development').collection('Users_Consumers').doc(customer_id).get()
										  .then(custSnapshot => {
											return cust_snap_data = custSnapshot.data();
										  }).catch(error => console.log('Couldnt get Customer details', error));	
	
	return Promise.all([OrderMenuref, orderDetailRef, consumerRef]).then(snapshot => {
		console.log("Snapshot",snapshot);
        var OrderMenu = snapshot[0];
        var OrederDetail = snapshot[1];
		var Consumer = snapshot[2];
		
		const projectId = admin.instanceId().app.options.projectId;
		const Storage = require('@google-cloud/storage');

		const PDFDocument = require ('pdfkit');
		const doc = new PDFDocument;
		const fs = require('fs');
		const uploadTo = '/restaurantOrders/restOrder_FAXCheck.pdf';	

		const gcs = new Storage({
		  projectId: projectId,
		  keyFilename: './credentials.json'
		});
		const bucket = gcs.bucket(bucketName);
		const file = bucket.file(uploadTo);

		var handlebars = require('handlebars');
		var readHTMLFile = function(path, callback) {
		fs.readFile(path, {encoding: 'utf-8'}, function (err, html) {
				if (err) {
					throw err;
					//callback(err);
				}
				else {
					return callback(null, html);
				}
			});
		};
		var pdf = require('html-pdf');

		readHTMLFile('./updated_receipt.html', function(err, html) {
			if (err) {
				console.log("Error in read html file",err);
			}

			var template = handlebars.compile(html);
			var replacements = {
				 customer_name: Consumer.name,
				 order_id: OrederDetail.order_id_show,
				 order_time: OrederDetail.order_date,
				 order_subtotal: OrederDetail.subTotal,
				 order_tax: OrederDetail.tax,
				 order_tip: 0,
				 order_total: OrederDetail.grandTotal,
				 products:products,
				 customer_mail: Consumer.emailID,
				 customer_phone: Consumer.mobileNumber,
				 customer_id: customer_id,	
				 order_eaten : "Eaten",
				 special_instructions : special_instructions,
				 customisation : '' 	
			};

			console.log("Replacement",replacements);
			var htmlToSend = template(replacements);

			var options = { 
				  format: 'A4', 
				  border: { top: "0.5cm", right: "0.5cm", bottom: "0.5cm", left: "0.5cm" }, 
				  timeout: 30000 
			};

			pdf.create(htmlToSend, options).toStream(function(err, stream) {	
				if(err){
					console.log("Error in toStream",err);
				}
				stream.pipe(file.createWriteStream());
			});

			//return p.then(function(buffers) {
				return file.getSignedUrl({
				  action: 'read',
				  expires: '03-09-2491'
				}).then(signedUrls => {
				  console.log("PDF Signed",signedUrls[0]);  
				  const pdfUrl = signedUrls[0];
					console.log("IN if condition ");                                            
					console.log("PDF Url",pdfUrl);
					/*if((inArray(['9325012281','9009241741','8983633095','9923673679','9922238594','9649478800','9823057870','9582211120','7058429017'], rest_snap_data.fax))){
					//faxNumber = "+91"+rest_snap_data.fax;	
				}else{
					//faxNumber = "+1"+rest_snap_data.fax;
				}*/
					//to: faxNumber,
					return client.fax.faxes.create({
						mediaUrl: pdfUrl,
						to: '+19082251401',
						from: twilioNumber
					},function(err,fax){
						if(err){
							console.log("Error while sending fax",err);
						}else{
							pdfCount = 1;
							console.log("fax Status",fax.sid);
						}
					});
				});
		});
		return console.log("Promise executed");
		
	}).catch(promiseError => console.log("Promise Error", promiseError));
 });

exports.restaurant_faxLive = functions.https.onRequest((request, response) => {
	
	var orderID = request.body.orderID;
	var customer_id = request.body.customerId;
	
	if(!(orderID) || orderID === ''){
	 	response.json({"status":"error","error": "Order ID is missing."});
		response.end(); 
	 }
	
	
	const promises = [];
	var order_data = [];
	var rest_id = '';
	var products = [];
	var order_eaten = '';
	var order_taken = '';
	var order_delivery = '';
	var order_id_show = '';
	var special_instructions = '';
	//var customisation = [];
	//var rest_snap_data;
	//var cust_snap_data;
	
	//var OrderMenuref = db.collection('Root').doc('Live').collection('Orders').doc('Orders_Menus').collection('Order_Menus').where('order_id', '==', orderID).get()
	var OrderMenuref = db.collection('Root').doc('Live').collection('Orders').doc('Orders_Details').collection('Order_Details').doc(orderID).collection('Order_Menus').get()
			  .then(ordSnapshot => {
				ordSnapshot.forEach(order => {
					order_data = order.data();
					products.push({product_name:order_data.name,product_count:order_data.count,product_price:order_data.price});
				});
				  return order_data;
			  }).catch(menuError => console.log('Couldnt get Order menu details', menuError));

								 
	var orderDetailRef = db.collection('Root').doc('Live').collection('Orders').doc('Orders_Details').collection('Order_Details').doc(orderID).get()
						  .then(orderdetails => {
							return order_detail_data = orderdetails.data();
						}).catch(orderError => console.log('Couldnt get order details', orderError));
	
	var consumerRef = db.collection('Root').doc('Live').collection('Users_Consumers').doc(customer_id).get()
										  .then(custSnapshot => {
											return cust_snap_data = custSnapshot.data();
										  }).catch(error => console.log('Couldnt get Customer details', error));	
	
	return Promise.all([OrderMenuref, orderDetailRef, consumerRef]).then(snapshot => {
		console.log("Snapshot",snapshot);
        var OrderMenu = snapshot[0];
        var OrederDetail = snapshot[1];
		var Consumer = snapshot[2];
		
		const projectId = admin.instanceId().app.options.projectId;
		const Storage = require('@google-cloud/storage');

		const PDFDocument = require ('pdfkit');
		const doc = new PDFDocument;
		const fs = require('fs');
		const uploadTo = '/restaurantOrders/restOrder_FAXCheck.pdf';	

		const gcs = new Storage({
		  projectId: projectId,
		  keyFilename: './credentials.json'
		});
		const bucket = gcs.bucket(bucketName);
		const file = bucket.file(uploadTo);

		var handlebars = require('handlebars');
		var readHTMLFile = function(path, callback) {
		fs.readFile(path, {encoding: 'utf-8'}, function (err, html) {
				if (err) {
					throw err;
					//callback(err);
				}
				else {
					return callback(null, html);
				}
			});
		};
		var pdf = require('html-pdf');

		readHTMLFile('./updated_receipt.html', function(err, html) {
			if (err) {
				console.log("Error in read html file",err);
			}

			var template = handlebars.compile(html);
			var replacements = {
				 customer_name: Consumer.name,
				 order_id: OrederDetail.order_id_show,
				 order_time: OrederDetail.order_date,
				 order_subtotal: OrederDetail.subTotal,
				 order_tax: OrederDetail.tax,
				 order_tip: 0,
				 order_total: OrederDetail.grandTotal,
				 products:products,
				 customer_mail: Consumer.emailID,
				 customer_phone: Consumer.mobileNumber,
				 customer_id: customer_id,	
				 order_eaten : "Eaten",
				 special_instructions : special_instructions,
				 customisation : '' 	
			};

			console.log("Replacement",replacements);
			var htmlToSend = template(replacements);

			var options = { 
				  format: 'A4', 
				  border: { top: "0.5cm", right: "0.5cm", bottom: "0.5cm", left: "0.5cm" }, 
				  timeout: 30000 
			};

			pdf.create(htmlToSend, options).toStream(function(err, stream) {	
				if(err){
					console.log("Error in toStream",err);
				}
				stream.pipe(file.createWriteStream());
			});

			//return p.then(function(buffers) {
				return file.getSignedUrl({
				  action: 'read',
				  expires: '03-09-2491'
				}).then(signedUrls => {
				  console.log("PDF Signed",signedUrls[0]);  
				  const pdfUrl = signedUrls[0];
					console.log("IN if condition ");                                            
					console.log("PDF Url",pdfUrl);
					/*if((inArray(['9325012281','9009241741','8983633095','9923673679','9922238594','9649478800','9823057870','9582211120','7058429017'], rest_snap_data.fax))){
					//faxNumber = "+91"+rest_snap_data.fax;	
				}else{
					//faxNumber = "+1"+rest_snap_data.fax;
				}*/
					//to: faxNumber,
					return client.fax.faxes.create({
						mediaUrl: pdfUrl,
						to: '+19082251401',
						from: twilioNumber
					},function(err,fax){
						if(err){
							console.log("Error while sending fax",err);
						}else{
							pdfCount = 1;
							console.log("fax Status",fax.sid);
						}
					});
				});
		});
		return console.log("Promise executed");
		
	}).catch(promiseError => console.log("Promise Error", promiseError));
 });

exports.menuDeal = functions.firestore
	.document('/Root/Development/deals/{dealId}')
    .onCreate((snap, context) => {
		const dealId = context.params.dealId;
		const dealValue = snap.data();
		var menu_id = dealValue.menu_id;
		var restaurant_id = dealValue.restaurant_id;
		var conusmerIds = [];
		var unique = require('array-unique');
		var restaurantLoction = [];
		
		//console.log("Deal",dealValue);
	
		var favMenuRef = db.collection('Root').doc('Development').collection('FavouriteMenuItems').where('MenuItemID', '==', menu_id).get()
			  .then(favMenuSnap => {
				favMenuSnap.forEach(favMenu => {
					favMenu_data = favMenu.data();
					conusmerIds.push({consumerId:favMenu_data.ConsumerID});
					//console.log("Menu consumer",conusmerIds);
				});
				  return console.log("Favorite Menu executed");
			  }).catch(menuError => console.log('Couldnt get Favourite Menu details', menuError));
	
		var favRestRef = db.collection('Root').doc('Development').collection('FavouriteRestaurants').where('RestaurantID', '==', restaurant_id).get()
			  .then(favRestSnap => {
				favRestSnap.forEach(favRest => {
					favRest_data = favRest.data();
					conusmerIds.push({consumerId:favRest_data.ConsumerID});
					//console.log("Restaurant consumer",conusmerIds);
				});
				  return console.log("Favorite Restaurant executed");
			  }).catch(restError => console.log('Couldnt get Favourite Restaurant details', restError));
	
		var restaurantRef = db.collection('Root').doc('Development').collection('Users_Restaurants').doc(restaurant_id)
						  .get()
						  .then(restSnapshot => {
							  var rest_snap_data = restSnapshot.data();
							  restaurantLoction.push(rest_snap_data);
						return console.log("Users Restaurant executed");
			  }).catch(restError => console.log('Couldnt get User Restaurant details', restError));
		//TO calculate distance between Restaurant and consumer => https://www.npmjs.com/package/geodist
		return Promise.all([favMenuRef, favRestRef, restaurantRef]).then(snapshot => {
			
			unique(conusmerIds);
			//console.log("Consumer Ids",conusmerIds);
			
			
			conusmerIds.forEach(consumer => {
				var customer_id = consumer.consumerId;
				//console.log("Consumer",customer_id);
				db.collection('Root').doc('Development').collection('Users_Consumers').doc(customer_id).collection(customer_id).doc('Current_Location')			  
					  .get()
					  .then(cstLocation => {
						//cstLocation.forEach(customer => {
								var customerLocation = cstLocation.data();
								var customerLatitude = customerLocation.Latitude;
								var customerLongitude = customerLocation.Longitude;
							
								console.log("rest_location",restaurantLoction);
								console.log("customerLatitude",customerLatitude);
								console.log("customerLongitude",customerLongitude);
								
								var rest_latitude = restaurantLoction[0].location.lattitude;
							  	var rest_longitude = restaurantLoction[0].location.longitude;
								
								var geodist = require('geodist');

								var dist = geodist({lat: rest_latitude, lon: rest_longitude}, {lat: customerLatitude, lon: customerLongitude});
								console.log("DIstance",dist);
						
							//if(dist < 20){

								db.collection('Root').doc('Development').collection('Users_Consumers').doc(customer_id).collection(customer_id).doc('tokenFCM').collection("IOS")			  
									  .get()
									  .then(fcmSnapshot => {
										fcmSnapshot.forEach(fcmChild => {
												var fcmtoken = fcmChild.data();
												var fcmKey = fcmChild.id;

												const payload = {
												  notification: {
														"title" : "New Deal Created",
														"body" : "New deal has been created"
													},
												  data : {
													"menuId" : menu_id
												  }
												};

												//console.log("IOS Payload", payload);

												admin.messaging().sendToDevice(fcmKey, payload);	
										});
										return console.log('Function executed successfully here');  
									  }).catch(pushError => console.log("Push Error", pushError));

								db.collection('Root').doc('Development').collection('Users_Consumers').doc(customer_id)			  
												  .get()
												  .then(customSnapshot => {
													  var custom_data = customSnapshot.data();
													  //console.log("User data",custom_data);
														if(custom_data.mobileNumber){

															var phoneNumber = '';
															if(inArray(['9325012281','9009241741','8983633095','9923673679','9922238594','9649478800','9823057870','9582211120','7058429017'], custom_data.mobileNumber)){
																phoneNumber = "+91"+custom_data.mobileNumber;
															}else{
																phoneNumber = "+1"+custom_data.mobileNumber;
															}

															if(phoneNumber){
																if ( !validE164(phoneNumber) ) {
																	throw new Error('number must be E164 format!')
																}

																const textMessage = {
																	body: 'New Deal has been created',
																	to: phoneNumber,
																	from: twilioNumber
																}
																//console.log('Consumer SMS',textMessage);
																client.messages.create(textMessage);
															}
														}
													 return console.log('Function executed successfully'); 
								  }).catch(messageError => console.log("Message Error", messageError));
							//}		
					//});
					return console.log("Consumer Location executed");
				}).catch(restError => console.log('Couldnt get consumer location details', restError));
		});
			return console.log("Promise executed");
		});
	//return Promise.all(promises);
});

exports.menuDealLive = functions.firestore
	.document('/Root/Live/deals/{dealId}')
    .onCreate((snap, context) => {
		const dealId = context.params.dealId;
		const dealValue = snap.data();
		var menu_id = dealValue.menu_id;
		var restaurant_id = dealValue.restaurant_id;
		var conusmerIds = [];
		var unique = require('array-unique');
		var restaurantLoction = [];
		
		//console.log("Deal",dealValue);
	
		var favMenuRef = db.collection('Root').doc('Live').collection('FavouriteMenuItems').where('MenuItemID', '==', menu_id).get()
			  .then(favMenuSnap => {
				favMenuSnap.forEach(favMenu => {
					favMenu_data = favMenu.data();
					conusmerIds.push({consumerId:favMenu_data.ConsumerID});
					//console.log("Menu consumer",conusmerIds);
				});
				  return console.log("Favorite Menu executed");
			  }).catch(menuError => console.log('Couldnt get Favourite Menu details', menuError));
	
		var favRestRef = db.collection('Root').doc('Live').collection('FavouriteRestaurants').where('RestaurantID', '==', restaurant_id).get()
			  .then(favRestSnap => {
				favRestSnap.forEach(favRest => {
					favRest_data = favRest.data();
					conusmerIds.push({consumerId:favRest_data.ConsumerID});
					//console.log("Restaurant consumer",conusmerIds);
				});
				  return console.log("Favorite Restaurant executed");
			  }).catch(restError => console.log('Couldnt get Favourite Restaurant details', restError));
	
		var restaurantRef = db.collection('Root').doc('Live').collection('Users_Restaurants').doc(restaurant_id)
						  .get()
						  .then(restSnapshot => {
							  var rest_snap_data = restSnapshot.data();
							  restaurantLoction.push(rest_snap_data);
						return console.log("Users Restaurant executed");
			  }).catch(restError => console.log('Couldnt get User Restaurant details', restError));
		//TO calculate distance between Restaurant and consumer => https://www.npmjs.com/package/geodist
		return Promise.all([favMenuRef, favRestRef, restaurantRef]).then(snapshot => {
			
			unique(conusmerIds);
			//console.log("Consumer Ids",conusmerIds);
			
			
			conusmerIds.forEach(consumer => {
				var customer_id = consumer.consumerId;
				//console.log("Consumer",customer_id);
				db.collection('Root').doc('Live').collection('Users_Consumers').doc(customer_id).collection(customer_id).doc('Current_Location')			  
					  .get()
					  .then(cstLocation => {
						//cstLocation.forEach(customer => {
								var customerLocation = cstLocation.data();
								var customerLatitude = customerLocation.Latitude;
								var customerLongitude = customerLocation.Longitude;
							
								console.log("rest_location",restaurantLoction);
								console.log("customerLatitude",customerLatitude);
								console.log("customerLongitude",customerLongitude);
								
								var rest_latitude = restaurantLoction[0].location.lattitude;
							  	var rest_longitude = restaurantLoction[0].location.longitude;
								
								var geodist = require('geodist');

								var dist = geodist({lat: rest_latitude, lon: rest_longitude}, {lat: customerLatitude, lon: customerLongitude});
								console.log("DIstance",dist);
						
							//if(dist < 20){

								db.collection('Root').doc('Live').collection('Users_Consumers').doc(customer_id).collection(customer_id).doc('tokenFCM').collection("IOS")			  
									  .get()
									  .then(fcmSnapshot => {
										fcmSnapshot.forEach(fcmChild => {
												var fcmtoken = fcmChild.data();
												var fcmKey = fcmChild.id;

												const payload = {
												  notification: {
														"title" : "New Deal Created",
														"body" : "New deal has been created"
													},
												  data : {
													"menuId" : menu_id
												  }
												};

												//console.log("IOS Payload", payload);

												admin.messaging().sendToDevice(fcmKey, payload);	
										});
										return console.log('Function executed successfully here');  
									  }).catch(pushError => console.log("Push Error", pushError));

								db.collection('Root').doc('Live').collection('Users_Consumers').doc(customer_id)			  
												  .get()
												  .then(customSnapshot => {
													  var custom_data = customSnapshot.data();
													  //console.log("User data",custom_data);
														if(custom_data.mobileNumber){

															var phoneNumber = '';
															if(inArray(['9325012281','9009241741','8983633095','9923673679','9922238594','9649478800','9823057870','9582211120','7058429017'], custom_data.mobileNumber)){
																phoneNumber = "+91"+custom_data.mobileNumber;
															}else{
																phoneNumber = "+1"+custom_data.mobileNumber;
															}

															if(phoneNumber){
																if ( !validE164(phoneNumber) ) {
																	throw new Error('number must be E164 format!')
																}

																const textMessage = {
																	body: 'New Deal has been created',
																	to: phoneNumber,
																	from: twilioNumber
																}
																//console.log('Consumer SMS',textMessage);
																client.messages.create(textMessage);
															}
														}
													 return console.log('Function executed successfully'); 
								  }).catch(messageError => console.log("Message Error", messageError));
							//}		
					//});
					return console.log("Consumer Location executed");
				}).catch(restError => console.log('Couldnt get consumer location details', restError));
		});
			return console.log("Promise executed");
		});
	//return Promise.all(promises);
});

exports.restaurant_profile = functions.https.onRequest((request, response) => {

	const MessagingResponse = require('twilio').twiml.MessagingResponse;
	console.log("Request",request.body.Body);
	const twiml = new MessagingResponse();
	//var dine_in_id = (request.body.Body).split("_");
	//var restCode = dine_in_id[0];
	//var tableCode = dine_in_id[1];
	var restCode = request.body.Body;
	const dataRequest = require('request-promise');
	var restaurantId = '';
	var restData =	db.collection('Root').doc('Development').collection('Users_Restaurants').where('res_dinein_ID', '==', restCode).get()
			  .then(RestSnap => {
				RestSnap.forEach(restSnap => {
					rest_data = restSnap.data();
					restaurantId = restSnap.id;
				});
				  return console.log("Restaurant Data executed");
			  }) .catch(function(error) {
				  	return console.log('In catch',error);
			});
	
	//"link": "https://fkrest.hurekadev.info?uid=" + restaurantId + ",table=" + tableCode,
	return Promise.all([restData]).then(snapshot => {
			if(restaurantId){
				var deeplink =  {
				  "dynamicLinkInfo": {
					"dynamicLinkDomain": "en5vz.app.goo.gl",
					"link": "https://fkrest.hurekadev.info?uid=" + restaurantId,
					"androidInfo": {
					  "androidPackageName": "foodapp.webcubator.com.foodappconsumer"
					},
					"iosInfo": {
					  "iosBundleId": "com.webcubator.foodAppConsumer",
					  "iosAppStoreId": "1400251621"
					}
				  }
				};

				const options = {
					method: "POST",
					uri: "https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyDIiQIa5ZTAfnrADbxkFrEwIFzbqkwRuOQ",
					body: deeplink,
					json: true
				};

				dataRequest(options)
					.then(function (parsedBody) {
						//console.log("parsebody ",parsedBody.shortLink);
						twiml.message('Please click here '+parsedBody.shortLink+' to view restaurant profile.');
						response.writeHead(200, {'Content-Type': 'text/xml'});
						response.end(twiml.toString());
						return parsedBody.shortLink;
					}).catch(function(error) {
						twiml.message('No restaurant found with this code. Please use different code.');
						response.writeHead(200, {'Content-Type': 'text/xml'});
						response.end(twiml.toString());
						return console.log('In link catch',error);
					});

				return console.log("Promise executed");

		}else{
			twiml.message('No restaurant found with this code. Please try again later.');
			response.writeHead(200, {'Content-Type': 'text/xml'});
			response.end(twiml.toString());	
			return console.log("Else executed");
		}
	});
 });

exports.restaurant_profileLive = functions.https.onRequest((request, response) => {

	const MessagingResponse = require('twilio').twiml.MessagingResponse;
	console.log("Request",request.body.Body);
	const twiml = new MessagingResponse();
	//var dine_in_id = (request.body.Body).split("_");
	//var restCode = dine_in_id[0];
	//var tableCode = dine_in_id[1];
	var restCode = request.body.Body;
	const dataRequest = require('request-promise');
	var restaurantId = '';
	var restData =	db.collection('Root').doc('Live').collection('Users_Restaurants').where('res_dinein_ID', '==', restCode).get()
			  .then(RestSnap => {
				RestSnap.forEach(restSnap => {
					rest_data = restSnap.data();
					restaurantId = restSnap.id;
				});
				  return console.log("Restaurant Data executed");
			  }) .catch(function(error) {
				  	return console.log('In catch',error);
			});
	//"link": "https://fkrest.hurekadev.info?uid=" + restaurantId + ",table=" + tableCode,
	return Promise.all([restData]).then(snapshot => {
			if(restaurantId){
				var deeplink =  {
				  "dynamicLinkInfo": {
					"dynamicLinkDomain": "en5vz.app.goo.gl",
					"link": "https://fkrest.hurekadev.info?uid=" + restaurantId,
					"androidInfo": {
					  "androidPackageName": "foodapp.webcubator.com.foodappconsumer"
					},
					"iosInfo": {
					  "iosBundleId": "com.webcubator.foodAppConsumer",
					  "iosAppStoreId": "1400251621"
					}
				  }
				};

				const options = {
					method: "POST",
					uri: "https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyDIiQIa5ZTAfnrADbxkFrEwIFzbqkwRuOQ",
					body: deeplink,
					json: true
				};

				dataRequest(options)
					.then(function (parsedBody) {
						//console.log("parsebody ",parsedBody.shortLink);
						twiml.message('Please click here '+parsedBody.shortLink+' to view restaurant profile.');
						response.writeHead(200, {'Content-Type': 'text/xml'});
						response.end(twiml.toString());
						return parsedBody.shortLink;
					}).catch(function(error) {
						twiml.message('No restaurant found with this code. Please use different code.');
						response.writeHead(200, {'Content-Type': 'text/xml'});
						response.end(twiml.toString());
						return console.log('In link catch',error);
					});

				return console.log("Promise executed");

		}else{
			twiml.message('No restaurant found with this code. Please try again later.');
			response.writeHead(200, {'Content-Type': 'text/xml'});
			response.end(twiml.toString());	
			return console.log("Else executed");
		}
	});
 });

exports.dineInMsg = functions.https.onRequest((request, response) => {
			
			var restaurantID = request.body.restaurantID;
			var customer_id = request.body.customerId;

			if(!(restaurantID) || restaurantID === ''){
				response.json({"status":"error","error": "Restaurant ID is missing."});
				response.end(); 
			 }
			
			const promises = [];
	
						promises.push(db.collection('Root').doc('Development').collection('Users_Restaurants').doc(restaurantID).collection(restaurantID).doc('tokenFCM').collection("Android")
						  .get()
						  .then(fcmSnapshot => {
							fcmSnapshot.forEach(fcmChild => {
									var fcmtoken = fcmChild.data();
									var fcmKey = fcmChild.id;
									db.collection('Root').doc('Development').collection('Users_Consumers').doc(customer_id)
									  .get()
									  .then(custSnapshot => {
											var cust_snap_data = custSnapshot.data();
											const payload = {
											  notification: {
													"title" : "Dine in Request",
													"body" : cust_snap_data.name+" has requested for Dine In"
												},
												  data : {
													"type" : "dineIn"
												  }
											};
											admin.messaging().sendToDevice(fcmKey, payload);	
									return console.log('Dine in Push executed successfully');
								}).catch(error => console.log('Couldnt send push notification', error));			
							});
							return console.log('Function executed successfully here');  
						  })
						);			  

		return Promise.all(promises);
});

exports.dineInMsgLive = functions.https.onRequest((request, response) => {
			
			var restaurantID = request.body.restaurantID;
			var customer_id = request.body.customerId;

			if(!(restaurantID) || restaurantID === ''){
				response.json({"status":"error","error": "Restaurant ID is missing."});
				response.end(); 
			 }
			
			const promises = [];
	
						promises.push(db.collection('Root').doc('Live').collection('Users_Restaurants').doc(restaurantID).collection(restaurantID).doc('tokenFCM').collection("Android")
						  .get()
						  .then(fcmSnapshot => {
							fcmSnapshot.forEach(fcmChild => {
									var fcmtoken = fcmChild.data();
									var fcmKey = fcmChild.id;
									db.collection('Root').doc('Live').collection('Users_Consumers').doc(customer_id)
									  .get()
									  .then(custSnapshot => {
											var cust_snap_data = custSnapshot.data();
											const payload = {
											  notification: {
													"title" : "Dine in Request",
													"body" : cust_snap_data.name+" has requested for Dine In"
												},
												  data : {
													"type" : "dineIn"
												  }
											};
											admin.messaging().sendToDevice(fcmKey, payload);	
									return console.log('Dine in Push executed successfully');
								}).catch(error => console.log('Couldnt send push notification', error));			
							});
							return console.log('Function executed successfully here');  
						  })
						);			  

		return Promise.all(promises);
});